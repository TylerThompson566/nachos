// Copyright (c) 1992-1993 The Regents of the University of California.
// Copyright (c) 1998 Rice University.
// Copyright (c) 2003 State University of New York at Stony Brook.
// All rights reserved.  See the COPYRIGHT file for copyright notice and
// limitation of liability and disclaimer of warranty provisions.

package nachos.kernel.userprog;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.devices.ConsoleDriver;
import nachos.kernel.filesys.OpenFile;
import nachos.machine.CPU;
import nachos.machine.Console;
import nachos.machine.MIPS;
import nachos.machine.NachosThread;
import nachos.machine.Simulation;

/**
 * Nachos system call interface.  These are Nachos kernel operations
 * 	that can be invoked from user programs, by trapping to the kernel
 *	via the "syscall" instruction.
 *
 * @author Thomas Anderson (UC Berkeley), original C++ version
 * @author Peter Druschel (Rice University), Java translation
 * @author Eugene W. Stark (Stony Brook University)
 */
public class Syscall {

    // System call codes -- used by the stubs to tell the kernel 
    // which system call is being asked for.

    /** Integer code identifying the "Halt" system call. */
    public static final byte SC_Halt = 0;

    /** Integer code identifying the "Exit" system call. */
    public static final byte SC_Exit = 1;

    /** Integer code identifying the "Exec" system call. */
    public static final byte SC_Exec = 2;

    /** Integer code identifying the "Join" system call. */
    public static final byte SC_Join = 3;

    /** Integer code identifying the "Create" system call. */
    public static final byte SC_Create = 4;

    /** Integer code identifying the "Open" system call. */
    public static final byte SC_Open = 5;

    /** Integer code identifying the "Read" system call. */
    public static final byte SC_Read = 6;

    /** Integer code identifying the "Write" system call. */
    public static final byte SC_Write = 7;

    /** Integer code identifying the "Close" system call. */
    public static final byte SC_Close = 8;

    /** Integer code identifying the "Fork" system call. */
    public static final byte SC_Fork = 9;

    /** Integer code identifying the "Yield" system call. */
    public static final byte SC_Yield = 10;

    /** Integer code identifying the "Remove" system call. */
    public static final byte SC_Remove = 11;
    
    /** Integer code identifying the "PredictCPU" system call. */
    public static final byte SC_PredictCPU = 12;
    
    /** Integer code identifying the "Print" system call */
    public static final byte SC_Print = 13;
    
    /** Integer code identifying the "Malloc" system call */
    public static final byte SC_Malloc = 14;
    
    /** Integer code identifying the "Free" system call */
    public static final byte SC_Free = 15;
    
    /** Integer return code for an error */
    public static final int RETURN_ERROR = -1;

    /**
     * Stop Nachos, and print out performance statistics.
     */
    public static void halt() {
	Debug.print('+', "Shutdown, initiated by user program.\n");
	Simulation.stop();
    }

    /* Address space control operations: Exit, Exec, and Join */

    /**
     * This user program is done.
     *
     * @param status Status code to pass to processes doing a Join().
     * status = 0 means the program exited normally.
     */
    public static void exit(int status) {
	
	//debug print
	Debug.println('+', "User program exits with status=" + status
				+ ": " + NachosThread.currentThread().name);
	
	//get the spaceID for the process exiting
	UserThread thread = (UserThread) NachosThread.currentThread();
	int spaceID = thread.space.getSpaceID();
	
	//go to the memory manager and free all of the pages
	Nachos.memoryManager.freeSpace(spaceID);
	
	//let joining threads know it exited
	Nachos.memoryManager.exit(spaceID);
	
	//finish the thread
	Nachos.scheduler.finishThread();
    }

    /**
     * Run the executable, stored in the Nachos file "name", and return the 
     * address space identifier.
     *
     * @param name The name of the file to execute.
     * @return the integer value (space ID) or RETURN_ERROR to represent an error
     */
    public static int exec(String name) {
	
	//debug print
	Debug.println('+', "syscall exec: " + name);
	
	//create the space and an open file
	AddrSpace space = new AddrSpace();
	OpenFile executable;

	//if we cannot open the file, then return an error code
	if((executable = Nachos.fileSystem.open(name)) == null) {
	    Debug.println('+', "Unable to open executable file: " + name);
	    Nachos.scheduler.finishThread();
	    return RETURN_ERROR;
	}
	
	//if we cannot load the program into memory, then return an error code
	if(space.exec(executable) == -1) {
	    Debug.println('+', "Unable to read executable file: " + name);
	    Nachos.scheduler.finishThread();
	    return RETURN_ERROR;
	}
	
	//Start ProgRun
	ProgRun.start(name, space);
	
	//add the process to the process list in the memory manager
	Nachos.memoryManager.addProcess(space.getSpaceID());
	
	//return the spaceID
	return space.getSpaceID();
    }

    /**
     * Wait for the user program specified by "id" to finish, and
     * return its exit status.
     *
     * @param id The "space ID" of the program to wait for.
     * @return the exit status of the specified program.
     */
    public static int join(int id) {
	
	//debug print
	Debug.print('+', "Syscall Join with ID " + id + "\n");
	
	//join the exiting thread
	Nachos.memoryManager.join(id);
	
	//return the exit status
	return 0;
    }


    /* File system operations: Create, Open, Read, Write, Close
     * These functions are patterned after UNIX -- files represent
     * both files *and* hardware I/O devices.
     *
     * If this assignment is done before doing the file system assignment,
     * note that the Nachos file system has a stub implementation, which
     * will work for the purposes of testing out these routines.
     */

    // When an address space starts up, it has two open files, representing 
    // keyboard input and display output (in UNIX terms, stdin and stdout).
    // Read and write can be used directly on these, without first opening
    // the console device.

    /** OpenFileId used for input from the keyboard. */
    public static final int ConsoleInput = 0;

    /** OpenFileId used for output to the display. */
    public static final int ConsoleOutput = 1;

    /**
     * Create a Nachos file with a specified name.
     *
     * @param name  The name of the file to be created.
     */
    public static void create(String name) { }

    /**
     * Remove a Nachos file.
     *
     * @param name  The name of the file to be removed.
     */
    public static void remove(String name) { }

    /**
     * Open the Nachos file "name", and return an "OpenFileId" that can 
     * be used to read and write to the file.
     *
     * @param name  The name of the file to open.
     * @return  An OpenFileId that uniquely identifies the opened file.
     */
    public static int open(String name) {return 0;}

    /**
     * Write "size" bytes from "buffer" to the open file.
     *
     * @param buffer Location of the data to be written.
     * @param size The number of bytes to write.
     * @param id The OpenFileId of the file to which to write the data.
     */
    public static void write(byte buffer[], int size, int id) {
	
	//debug print
	Debug.print('+', "Syscall write to ID " + id + "\n");
	
	//if we are outputting to a console
	if (id == ConsoleOutput) {
	    
	    //if the thread does not have a console ID (0) then give it one
	    if (((UserThread) (NachosThread.currentThread())).consoleID == 0) {
		((UserThread) (NachosThread.currentThread())).consoleID = Nachos.consoleManager.assignConsoleID();
		
		//if there are no available consoles, then do nothing
		if (((UserThread) (NachosThread.currentThread())).consoleID == 0) {
		    Debug.println('+', "No Consoles available.");
		    return;
		}
	    }
	    
	    //get the console. if the console is null after retrieving it, then do nothing
	    Console console = Nachos.consoleManager.getConsole(((UserThread) (NachosThread.currentThread())).consoleID);
	    if (console == null) {
		Debug.println('+', "No Console to write to.");
		return;
	    }
	    
	    //create a driver to maintain the console
	    ConsoleDriver driver = new ConsoleDriver(console);
	    
	    //write to the console
	    for(int i = 0; i < size; i++) {
		driver.putChar((char)buffer[i]);
		
		//if the character is a newline character, move the cursor to the left of the console
		if ((char)buffer[i] == '\n') {
		    driver.putChar('\r');
		}
		
		//sleep for a bit to allow the driver to ready itself for the next input
		Nachos.scheduler.sleepThread(10);
	    }
	    
	    
	    //stop the console
	    //driver.stop();
	    
	    //tell the console manager that we are no longer using the console
	    Nachos.consoleManager.setConsoleUnused(((UserThread) (NachosThread.currentThread())).consoleID);
	    ((UserThread) (NachosThread.currentThread())).consoleID = 0;
	    
	}
    }

    /**
     * Read "size" bytes from the open file into "buffer".  
     * Return the number of bytes actually read -- if the open file isn't
     * long enough, or if it is an I/O device, and there aren't enough 
     * characters to read, return whatever is available (for I/O devices, 
     * you should always wait until you can return at least one character).
     *
     * @param buffer Where to put the data read.
     * @param size The number of bytes requested.
     * @param id The OpenFileId of the file from which to read the data.
     * @return The actual number of bytes read.
     */
    public static int read(byte buffer[], int size, int id) {
	
	//debug print
	Debug.print('+', "Syscall read from ID " + id + "\n");
	
	//the number of bytes read
	int bytesRead = 0;
	
	//if we are reading from the console
	if (id == ConsoleInput) {
	    
	    //if the thread does not have a console ID (0) then give it one
	    if (((UserThread) (NachosThread.currentThread())).consoleID == 0) {
		((UserThread) (NachosThread.currentThread())).consoleID = Nachos.consoleManager.assignConsoleID();
		
		//if there are no available consoles, then do nothing
		if (((UserThread) (NachosThread.currentThread())).consoleID == 0) {
		    Debug.println('+', "No Consoles available.");
		    return 0;
		}
	    }

	    //get the console. if the console is null after retrieving it, then do nothing
	    Console console = Nachos.consoleManager.getConsole(((UserThread) (NachosThread.currentThread())).consoleID);
	    if (console == null) {
		Debug.println('+', "No Console to read from.");
		return 0;
	    }
	    
	    //create a driver to maintain the console
	    ConsoleDriver driver = new ConsoleDriver(console);
	    
	    //create a counter
	    int charCounter = 0;
	    
	    //loop infinitely while reading characters
	    while (true) {
		
		if (charCounter == buffer.length) {
		    break;
		}
		
		//get the character, echo it, then put it in the buffer
		char ch = driver.getChar();
		driver.putChar(ch);
		
		//if the character is a backspace, then go back a character in the buffer if possible
		if (ch == '\b') {
		    
		    //set the current location in the buffer to 0
		    buffer[charCounter] = 0;
		    
		    //decrement the character counter if its over 0
		    if (charCounter != 0) {
			charCounter--;
		    }
		}
		
		else {
		    buffer[charCounter] = (byte) ch;
		    
		    //if we hit newline, make it look good on the console by putting the cursor back to the left
		    if(ch == '\n') {
			driver.putChar('\r');
		    }
		    
		    //increment the character counter
		    charCounter++;
		}
	    }
	    
	    //stop the console
	    driver.stop();
	    
	    //set the bytes read to the charCounter
	    bytesRead = charCounter;
	    
	    //tell the console manager that we are no longer using the console
	    Nachos.consoleManager.setConsoleUnused(((UserThread) (NachosThread.currentThread())).consoleID);
	    ((UserThread) (NachosThread.currentThread())).consoleID = 0;
	    
	    
	    /*
	    System.out.println("READ-TEST");
	    System.out.println("-----------------------------------------------------------");
	    for (int i = 0;i < buffer.length;i++) {
		System.out.print((char) buffer[i]);
	    }
	    System.out.println("\n-----------------------------------------------------------");
	    */
	    
	}
	
	//return the number of bytes read
	return bytesRead;
	}

    /**
     * Close the file, we're done reading and writing to it.
     *
     * @param id  The OpenFileId of the file to be closed.
     */
    public static void close(int id) {}


    /*
     * User-level thread operations: Fork and Yield.  To allow multiple
     * threads to run within a user program. 
     */

    /**
     * Fork a thread to run a procedure ("func") in the *same* address space 
     * as the current thread.
     *
     * @param func The user address of the procedure to be run by the
     * new thread.
     */
    public static void fork(int func) {
	
	//debug print
	Debug.println('+', "syscall fork: " + NachosThread.currentThread().name);
	
	//get a copy of the space
	AddrSpace space = new AddrSpace(((UserThread) NachosThread.currentThread()).space);
	
	//allocate a new stack for the space, if it doesn't work, do nothing
	if (space.allocNewStack() != 0) {
	    return;
	}
	
	//start the forked thread
	ProgFork.start(((UserThread) NachosThread.currentThread()).name, space, func);
    }

    /**
     * Yield the CPU to another runnable thread, whether in this address space 
     * or not. 
     */
    public static void yield() {
	
	//debug print
	Debug.print('+', "Syscall yield\n");
	
	//yield the thread so another thread can run
	Nachos.scheduler.yieldThread();
    }
    
    /**
     * Predict the CPU burst length
     */
    public static void predictCPU(int ticks) {
	
	Debug.println('+', "Syscall predictCPU");
	
	//get the current user thread
	UserThread currentThread = ((UserThread) NachosThread.currentThread());
	
	//calculate the burst time and save it to the thread
	currentThread.burstTime = ticks;
	
	//yield the thread so we can reschedule based on the new priorities
	if (Nachos.scheduler.getSchedulingMethod() == Nachos.scheduler.SHORTEST_PROCESS_NEXT || 
	    Nachos.scheduler.getSchedulingMethod() == Nachos.scheduler.SHORTEST_REMAINING_TIME_FIRST ||
	    Nachos.scheduler.getSchedulingMethod() == Nachos.scheduler.HIGHEST_RESPONSE_RATIO_NEXT) {
	    Debug.println('t', "found new priority, switching contexts");
	    Nachos.scheduler.yieldThread();
	}
    }
    
    /**
     * Print
     */
    public static void print(int print) {
	System.out.println(print);
    }
}


/**
 * Create a new thread that will run under the space provided
 * @author Tyler
 *
 */
class ProgFork implements Runnable {
    
    /** the space that the forked process will run under */
    private AddrSpace space;
    
    /** the pointer where the execution should start */
    private int func;
    
    public ProgFork(String name, AddrSpace space, int func) {
	
	//initialize the space and pointer
	this.space = space;
	this.func = func;
	
	//create the UserThread and set it ready to run
	UserThread thread = new UserThread(name, this, space);
	Nachos.scheduler.readyToRun(thread);
    }
    
    public void run() {
	space.restoreState();			// load page table register

	CPU.writeRegister(MIPS.PCReg, func);	// write to the register the pointer to start reading to
	CPU.runUserCode();			// jump to the user program
	Debug.ASSERT(false);			// machine->Run never returns;
	// the address space exits
	// by doing the syscall "exit"
    }
    
    public static void start(String name, AddrSpace space, int func) {
	new ProgFork(name, space, func);
    }
}