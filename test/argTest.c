/* 
 * tests initial arguments to main
 */

#include "syscall.h"

int main(int n) {
    Print(n);
    Exit(0);
}
