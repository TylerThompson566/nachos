// DiskDriver.java
//	Class for synchronous access of the disk.  The physical disk 
//	is an asynchronous device (disk requests return immediately, and
//	an interrupt happens later on).  This is a layer on top of
//	the disk providing a synchronous interface (requests wait until
//	the request completes).
//
//	Uses a semaphore to synchronize the interrupt handlers with the
//	pending requests.  And, because the physical disk can only
//	handle one operation at a time, uses a lock to enforce mutual
//	exclusion.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// Copyright (c) 1998 Rice University.
// Copyright (c) 2003 State University of New York at Stony Brook.
// All rights reserved.  See the COPYRIGHT file for copyright notice and 
// limitation of liability and disclaimer of warranty provisions.

package nachos.kernel.devices;


import nachos.Debug;
import nachos.machine.Machine;
import nachos.machine.NachosThread;
import nachos.util.DiskQueue;
import nachos.machine.Disk;
import nachos.machine.InterruptHandler;
import nachos.kernel.Nachos;
import nachos.kernel.threads.Semaphore;


/**
 * This class defines a "synchronous" disk abstraction.
 * As with other I/O devices, the raw physical disk is an asynchronous
 * device -- requests to read or write portions of the disk return immediately,
 * and an interrupt occurs later to signal that the operation completed.
 * (Also, the physical characteristics of the disk device assume that
 * only one operation can be requested at a time).
 *
 * This driver provides the abstraction of "synchronous I/O":  any request
 * blocks the calling thread until the requested operation has finished.
 * 
 * @author Thomas Anderson (UC Berkeley), original C++ version
 * @author Peter Druschel (Rice University), Java translation
 * @author Eugene W. Stark (Stony Brook University)
 */
public class DiskDriver {

    /** Raw disk device. */
    private Disk disk;
    
    /** The queue of requests that it is working on */
    private DiskQueue<DiskRequest> queue;
    
    /** the current request the disk is working on */
    private DiskRequest currentRequest;
    
    /** the semaphore that will ensure atomic access to the queue */
    private Semaphore queueSem;
    
    /**
     * Initialize the synchronous interface to the physical disk, in turn
     * initializing the physical disk.
     * 
     * @param unit  The disk unit to be handled by this driver.
     */
    public DiskDriver(int unit) {
	disk = Machine.getDisk(unit);
	disk.setHandler(new DiskIntHandler());
	if (Nachos.options.DISK_SCHEDULE_METHOD == Nachos.options.CSCAN) {
	    queue = new DiskCSCANQueue<DiskRequest>();
	}
	else {
	    queue = new DiskFCFSQueue<DiskRequest>();
	}
	currentRequest = null;
	queueSem = new Semaphore("Disk Driver Queue Semaphore", 1);
    }

    /**
     * Get the total number of sectors on the disk.
     * 
     * @return the total number of sectors on the disk.
     */
    public int getNumSectors() {
	return disk.geometry.NumSectors;
    }

    /**
     * Get the sector size of the disk, in bytes.
     * 
     * @return the sector size of the disk, in bytes.
     */
    public int getSectorSize() {
	return disk.geometry.SectorSize;
    }
    
    /** get the current request the disk is working on */
    public DiskRequest getCurrentRequest() {
	return currentRequest;
    }
    
    /** set the current request the disk is working on */
    public void setCurrentRequest(DiskRequest currentRequest) {
	this.currentRequest = currentRequest;
    }
    
    /** get a string representation of the disk scheduling method */
    public String getSchedulingMethod() {
	if (Nachos.options.DISK_SCHEDULE_METHOD == Nachos.options.FCFS) {
	    return "First Come, First Served.";
	}
	return "CSCAN";
    }
    
    /**
     * Read the contents of a disk sector into a buffer.  Return only
     *	after the data has been read.
     *
     * @param sectorNumber The disk sector to read.
     * @param data The buffer to hold the contents of the disk sector.
     * @param index Offset in the buffer at which to place the data.
     */
    public void readSector(int sectorNumber, byte[] data, int index) {
	
	//ensure the area of the disk exists
	Debug.ASSERT(0 <= sectorNumber && sectorNumber < getNumSectors());
	
	//create a new semaphore that will be used to block the requesting thread
	Semaphore sem = new Semaphore("disk read request semaphore for " + NachosThread.currentThread().name, 0);
	
	//create a new request
	DiskRequest request = new DiskRequest(true, sectorNumber, data, index, sem);
		
	//handle the request
	handleRequest(request);
    }

    /**
     * Write the contents of a buffer into a disk sector.  Return only
     *	after the data has been written.
     *
     * @param sectorNumber The disk sector to be written.
     * @param data The new contents of the disk sector.
     * @param index Offset in the buffer from which to get the data.
     */
    public void writeSector(int sectorNumber, byte[] data, int index) {
	
	//ensure the area of the disk exists
	Debug.ASSERT(0 <= sectorNumber && sectorNumber < getNumSectors());
	
	//create a new semaphore that will be used to block the requesting thread
	Semaphore sem = new Semaphore("disk write request semaphore for " + NachosThread.currentThread().name, 0);
	
	//create a new request
	DiskRequest request = new DiskRequest(false, sectorNumber, data, index, sem);
	
	//handle the request
	handleRequest(request);
    }
    
    /**
     * Request to read/write something to the disk
     * @param request
     */
    public void handleRequest(DiskRequest request) {
	
	//add the request to the queue
	queueSem.P();
	queue.offer(request);
	queueSem.V();
	
	//if the disk is doing nothing, then get the disk to start reading/writing
	if (currentRequest == null) {
	    
	    //set the current request to this request while removing it from the list at the same time
	    queueSem.P();
	    currentRequest = queue.poll();
	    queueSem.V();
	    
	    //read/write
	    if (currentRequest.read) {
		disk.readRequest(currentRequest.sectorNumber, currentRequest.data, currentRequest.index);
	    }
	    else {
		disk.writeRequest(currentRequest.sectorNumber, currentRequest.data, currentRequest.index);
	    }
	}
		
	//wait for the disk to finish writing
	request.sem.P();
    }

    /**
     * DiskDriver interrupt handler class.
     */
    private class DiskIntHandler implements InterruptHandler {
	
	/**
	 * When the disk interrupts, just wake up the thread that issued
	 * the request that just finished.
	 */
	public void handleInterrupt() {
	    
	    //wake up the waiting thread
	    currentRequest.sem.V();
	    
	    //set the next request, if it is not null, then do it
	    queueSem.P();
	    currentRequest = queue.poll();
	    queueSem.V();
	    if (currentRequest != null) {
		if (currentRequest.read) {
		    disk.readRequest(currentRequest.sectorNumber, currentRequest.data, currentRequest.index);
		}
		else {
		    disk.writeRequest(currentRequest.sectorNumber, currentRequest.data, currentRequest.index);
		}
	    }
	}
    }
}