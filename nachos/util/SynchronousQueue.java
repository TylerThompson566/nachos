package nachos.util;

import java.util.LinkedList;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.threads.Semaphore;
import nachos.kernel.threads.SpinLock;
import nachos.machine.CPU;
import nachos.util.Queue;

/**
 * This class is patterned after the SynchronousQueue class
 * in the java.util.concurrent package.
 *
 * A SynchronousQueue has no capacity: each insert operation
 * must wait for a corresponding remove operation by another
 * thread, and vice versa.  A thread trying to insert an object
 * enters a queue with other such threads, where it waits to
 * be matched up with a thread trying to remove an object.
 * Similarly, a thread trying to remove an object enters a
 * queue with other such threads, where it waits to be matched
 * up with a thread trying to insert an object.
 * If there is at least one thread waiting to insert and one
 * waiting to remove, the first thread in the insertion queue
 * is matched up with the first thread in the removal queue
 * and both threads are allowed to proceed, after transferring
 * the object being inserted to the thread trying to remove it.
 * At any given time, the <EM>head</EM> of the queue is the
 * object that the first thread on the insertion queue is trying
 * to insert, if there is any such thread, otherwise the head of
 * the queue is null.
 */

public class SynchronousQueue<T> implements Queue<T> {
    
    /**
     * Spin lock to synchronize thread access to queue internal state
     * against interrupt handler (i.e. callouts) being executed on
     * another CPU.
     */
    private SpinLock spinlock;
    
    /** Queue of pending puts. */
    private LinkedList<PutEntry> putQueue;
    
    /** Queue of pending takes. */
    private LinkedList<TakeEntry> takeQueue;
    
    /**
     * Initialize a new SynchronousQueue object.
     */
    public SynchronousQueue() {
	spinlock = new SpinLock("SynchronousQueue spinlock");
	putQueue = new LinkedList<PutEntry>();
	takeQueue = new LinkedList<TakeEntry>();
    }
    
    /**
     * Acquire the exclusion necessary to access the queues.
     * 
     * @return the interrupt level at the time of the call.
     */
    private int beginCritical() {
	// It turns out than no normal Lock is needed, because anywhere
	// we access the queues we have to have interrupts disabled anyway
	// (because the timeout handlers access the queues), so that means
	// that no other thread can run on this CPU, and since we also already
	// need to spinlock against timer handlers on other CPUs,
	// that is sufficient to lock out threads on other CPUs as well.
	int oldLevel = CPU.setLevel(CPU.IntOff);
	spinlock.acquire();
	return oldLevel;
    }
    
    /**
     * Release the exclusion acqured by beginCritical.
     * 
     * @param oldLevel  The interrupt level to return to.
     */
    private void endCritical(int oldLevel) {
	spinlock.release();
	CPU.setLevel(oldLevel);
    }

    /**
     * Adds the specified object to this queue,
     * waiting if necessary for another thread to remove it.
     *
     * @param obj The object to add.
     * @param timeout  Number of ticks to wait before timing out and failing,
     * or -1 to wait indefinitely.  A value of 0 means fail immediately if
     * there is no matching take.
     * @return true if the object was successfully put into the queue,
     * otherwise false.
     */
    public boolean put(T obj, int timeout) {
	int oldLevel = beginCritical();
	TakeEntry take = takeQueue.poll();
	// We might need to add an entry to the put queue.
	// We have to hold the exclusion until then, otherwise an observation
	// that the take queue is empty will no longer be valid.
	if(take != null) {
	    // There is a taker, give him the object and notify him.
	    take.timedOut--;  // Prevent entry from being timed out.
	    endCritical(oldLevel);
	    take.object = obj;
	    take.sem.V();
	    return true;
	} else {
	    // There are no takers, either time out immediately or else put
	    // an entry in the put queue and block.
	    if(timeout == 0) {
		endCritical(oldLevel);
		return false;
	    } else {
		final PutEntry put = new PutEntry(obj);
		putQueue.offer(put);
		// Now that the entry is in the put queue, it is OK to release
		// the exclusion.
		endCritical(oldLevel);
		
		// If a timeout was specified, then arrange to time out
		// after the specified time has elapsed.
		if(timeout > 0) {
		    Nachos.callout.schedule
		    	(new Runnable() {
		    	    public void run() {
		    		// Remove put if it still happens to be in the queue.
		    		// We need exclusion for this.
		    		int oldLevel = beginCritical();
		    		putQueue.remove(put);
		    		
		    		// Mark it as timed out and notify the owner.
		    		if(put.timedOut == 0)
		    		    put.timedOut++;
		    		endCritical(oldLevel);
		    		put.sem.V();
		    	    }
		    	}, timeout);
		}
		
		// Wait for a matching take to arrive, or to time out.
		put.sem.P();
		Debug.ASSERT(put.timedOut != 0);
		return put.timedOut < 0;
	    }
	}
   }

    /**
     * Adds the specified object to this queue,
     * waiting if necessary for another thread to remove it.
     *
     * @param e The object to add.
     * @return true if the object was successfully put into the queue,
     * otherwise false.
     */
    public boolean put(T e) {
	return put(e, -1);
    }
    
    /**
     * Adds an element to this queue, if there is a thread currently
     * waiting to remove it, otherwise returns immediately.
     * 
     * @param e  The element to add.
     * @return  true if the element was successfully added, false if the element
     * was not added.
     */
    @Override
    public boolean offer(T e) {
	return put(e, 0);
    }
    
    /**
     * Retrieves and removes the head of this queue,
     * waiting if necessary for another thread to insert it.
     *
     * @param timeout  Number of ticks to wait before timing out and failing,
     * or -1 to wait indefinitely.  A value of 0 means fail immediately if
     * there is no matching put.
     * @return the head of this queue, or null if wait is false and there
     * is no matching put.
     */
    public T take(int timeout) {
	int oldLevel = beginCritical();
	PutEntry put = putQueue.poll();
	// We might need to add an entry to the take queue.
	// We have to hold the exclusion until then, otherwise an observation
	// that the put queue is empty will no longer be valid.
	if(put != null) {
	    // There is a putter, get his object and notify him.
	    put.timedOut--;  // Prevent entry from being timed out.
	    endCritical(oldLevel);
	    put.sem.V();
	    return put.object;
	} else {
	    // There are no putters, either time out immediately or else put
	    // an entry in the take queue and block.
	    if(timeout == 0) {
		endCritical(oldLevel);
		return null;
	    } else {
		final TakeEntry take = new TakeEntry();
		takeQueue.offer(take);
		// Now that the entry is in the take queue, it is OK to release
		// the exclusion.
		endCritical(oldLevel);
		
		// If a timeout was specified, then arrange to time out
		// after the specified time has elapsed.
		if(timeout > 0) {
		    Nachos.callout.schedule
		    	(new Runnable() {
		    	    public void run() {
		    		// Remove take if it still happens to be in the queue.
		    		// We need exclusion for this.
		    		int oldLevel = beginCritical();
		    		takeQueue.remove(take);
		    		
		    		// Mark it as timed out and notify the owner.
		    		if(take.timedOut == 0)
		    		    take.timedOut++;
		    		endCritical(oldLevel);
		    		take.sem.V();
		    	    }
		    	}, timeout);
		}
		
		// Wait for a matching put to arrive, or to time out.
		take.sem.P();
		Debug.ASSERT(take.timedOut != 0);
		if(take.timedOut > 0)
		    return null;
		else
		    return take.object;
	    }
	}
    }

    /**
     * Retrieves and removes the head of this queue,
     * waiting if necessary for another thread to insert it.
     *
     * @return the head of this queue.
     */
    public T take() {
	return take(-1);
    }
    
    /**
     * Retrieves and removes the head of this queue, if another thread
     * is currently making an element available.
     * 
     * @return  the head of this queue, or null if no element is available.
     */
    @Override
    public T poll() {
	return take(0);
    }
    
    /**
     * Always returns null.
     *
     * @return  null
     */
    @Override
    public T peek() { return null; }
    
    /**
     * Always returns true.
     * 
     * @return true
     */
    @Override
    public boolean isEmpty() { return true; }

    // The following methods are to be implemented for the second
    // part of the assignment.

    /**
     * Adds an element to this queue, waiting up to the specified
     * timeout for a thread to be ready to remove it.
     * 
     * @param e  The element to add.
     * @param timeout  The length of time (in "ticks") to wait for a
     * thread to be ready to remove the element, before giving up and
     * returning false.
     * @return  true if the element was successfully added, false if the element
     * was not added.
     */
    public boolean offer(T e, int timeout) {
	return put(e, timeout);
    }
    
    /**
     * Retrieves and removes the head of this queue, waiting up to the
     * specified timeout for a thread to make an element available.
     * 
     * @param timeout  The length of time (in "ticks") to wait for a
     * thread to make an element available, before giving up and returning
     * true.
     * @return  the head of this queue, or null if no element is available.
     */
    public T poll(int timeout) {
	return take(timeout);
    }
    
    /**
     * Class whose objects represent pending calls to put.
     */
    private class PutEntry {
	
	/** Semaphore on which to wait for matching take. */
	public final Semaphore sem;
	
	/** Object being transferred. */
	public final T object;
	
	/**
	 * Whether a timeout has occurred.
	 * >0 means timeout, <0 means success, 0 not yet decided.
	 */
	public volatile int timedOut;
	
	public PutEntry(T obj) {
	    sem = new Semaphore("PutEntry", 0);
	    object = obj;
	}
	
    }
    
    /**
     * Class whose objects represent pending calls to take.
     */
    private class TakeEntry {
	
	/** Semaphore on which to wait for matching put. */
	public final Semaphore sem;
	
	/** Object being transferred. */
	public T object;
	
	/**
	 * Whether a timeout has occurred.
	 * >0 means timeout, <0 means success, 0 not yet decided.
	 */
	public volatile int timedOut;
	
	public TakeEntry() {
	    sem = new Semaphore("TakeEntry", 0);
	}
	
    }

}
