/* 
 * Stallings Test
 * 1 instruction per tick
 * 12 instructions per iteration
 * 12 ticks per iteration
 */

#include "syscall.h"

int main(int n) {
    int i;
    PredictCPU(12 * n);
    while (i < n) {
    	i++;
    }
    Exit(0);
}
