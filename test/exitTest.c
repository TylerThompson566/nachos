/* Basic test of Exit() system call */

#include "syscall.h"

int
main()
{
	Exit(0);
}
