package nachos.kernel.devices;

import nachos.kernel.threads.Semaphore;
import nachos.machine.Console;
import nachos.machine.Machine;

/**
 * The Console Manager ensures that each user thread has only 1 console associated with it.
 * The console manager also maintains the use of all of them, whether they close and so on.
 * @author Tyler
 *
 */
public class ConsoleManager {
    
    /** array telling us the status of all consoles
     * 0 = unused, otherwise the number is the ID of the userThread that is using
     * the console represented by the index. */
    boolean[] consoleList;
    
    /** semaphore for maintaining atomic access to the list */
    Semaphore consoleListSem;
    
    /** Constructor for the console manager */
    public ConsoleManager() {
	
	//initialize the console list semaphore
	consoleListSem = new Semaphore("ConsoleListSemaphore", 1);
	
	//initialize the console list and atomically set all of the entries to false
	consoleList = new boolean[Machine.NUM_CONSOLES];
	consoleListSem.P();
	for (int i = 0;i < Machine.NUM_CONSOLES;i++) {
	    consoleList[i] = false;
	}
	consoleListSem.V();
    }
    
    /** get a console that is not in use, and return it 
     * @param id The ID of the console we wish to get
     * @return the console we are using*/
    public Console getConsole(int id) {
	
	//return the console that the ID is associated with
	return Machine.getConsole(id);
    }
    
    /** Assign a consoleID to a userThread 
     * @return 0 if all consoles are in use, the index of the unused console otherwise.*/
    public int assignConsoleID() {
	
	//create an int which will be returned
	int id = 0;
	
	//atomically iterate through the list
	//start at 1 so we do not use the general purpose console
	consoleListSem.P();
	for (int i = 1;i < consoleList.length;i++) {
	    
	    //if we find an unused console then we can get the ID for that console
	    if (consoleList[i] == false) {
		consoleList[i] = true;
		id = i;
		break;
	    }
	}
	consoleListSem.V();
	
	//return the ID
	return id;
    }
    
    /** Set a console no longer in use */
    public void setConsoleUnused(int id) {
	
	//atomically set the console to unused
	consoleListSem.P();
	consoleList[id] = false;
	consoleListSem.V();
    }
}
