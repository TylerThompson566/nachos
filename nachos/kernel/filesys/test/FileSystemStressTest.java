package nachos.kernel.filesys.test;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.filesys.OpenFile;
import nachos.machine.NachosThread;
import nachos.kernel.filesys.FileSystemReal;

public class FileSystemStressTest implements Runnable {
    
    private final int READ = 0;
    private final int WRITE = 1;
    private final int CREATE = 2;
    private final int DELETE = 3;
    private int id;
    private byte[] buffer;
    private String filename;
    private OpenFile file;
    private int totalThreads;
    
    /**
     * initialize an instance of the stress test and schedule it to run
     * @param id the id for the thread
     */
    public FileSystemStressTest(int id, int totalThreads) {
	this.id = id;
	this.totalThreads = totalThreads;
	buffer = new byte[10];
	Nachos.fileSystem.create("file0", 200);
	Nachos.fileSystem.create("file1", 200);
	Nachos.fileSystem.create("file2", 200);
	Nachos.fileSystem.create("file3", 200);
	NachosThread t = new NachosThread("File System Stress Test Thread " + id, this);
	Nachos.scheduler.readyToRun(t);
    }
    
    /**
     * read, write, create, and delete files
     */
    public void run() {
	
	//loop 10 times
	for (int i = 0;i < 10;i++) {
	    
	    //decide what action to do
	    int choice = getRandom();
	    
	    //debug print
	    Debug.println('+', "thread " + id + " iteration " + (i + 1) + " (" + getChoiceString(choice) + ")");
	    
	    if (choice == READ) {
		readFile();
	    }
	    else if (choice == WRITE) {
		writeFile();
	    }
	    else if (choice == CREATE) {
		createFile();
	    }
	    else if (choice == DELETE) {
		deleteFile();
	    }
	}
	
	//consistency check
	Nachos.fileHeaderManager.threadsFinished++;
	if (Nachos.fileHeaderManager.threadsFinished == totalThreads) {
	    ((FileSystemReal) Nachos.fileSystem).checkConsistency();
	}
	
	//end the thread
	Nachos.scheduler.finishThread();
    }
    
    /**
     * get a random number between 0 and 3
     */
    private int getRandom() {
	double random = Math.random();
	random  = random * 100;
	int casted = (int) random;
	int answer = 0;
	for (int i = 0;i < casted;i++) {
	    answer++;
	    if (answer >= 4) {
		answer = 0;
	    }
	}
	return answer;
    }
    
    /**
     * read a file
     */
    private void readFile() {
	
	//choose between 0 and 3 again to decide what file to read
	int choice = getRandom();
	filename = new String("file" + choice);
	    	    
	//open the file
	file = Nachos.fileSystem.open(filename);
	if (file == null) {
	    Debug.println('+', "thread " + id + " could not open " + filename);
	    return;
	}
	    	    
	//clear the buffer
	for (int k = 0;k < 10;k++) {
	    buffer[k] = 0;
	}
	    	    
	//read the file and print the output
	file.readAt(buffer, 0, 10, 0);
	System.out.print("thread " + id + " read " + filename + ". output:\n\t");
	for (int k = 0;k < 10;k++) {
	    System.out.print((char) buffer[k]);
	}
	Debug.println('+',"");
	    	    
	//close the file
	file.close();
    }
    
    /**
     * write to a file
     */
    private void writeFile() {
	
	//choose between 0 and 3 again to decide what file to write to
	int choice = getRandom();
	filename = new String("file" + choice);
	    	    
	//open the file
	file = Nachos.fileSystem.open(filename);
	if (file == null) {
	    Debug.println('+', "thread " + id + " could not open " + filename);
	    return;
	}
	    
	//write to the buffer
	writeBuffer();
	    	    
	//read the file and print the output
	int bytesWritten = file.write(buffer, 0, 10);
	Debug.println('+', "thread " + id + " wrote to " + filename + " (" + bytesWritten + " bytes)");
	    	    
	//close the file
	file.close();
    }
    
    /**
     * create a new file
     */
    private void createFile() {
	
	//choose between 0 and 1 again to decide what file to create
	int choice = getRandom();
	filename = new String("file" + choice);
	    	    
	//create the file
	if (!Nachos.fileSystem.create(filename, 200)) {
	    Debug.println('+', "thread " + id + " could not create " + filename + ".");
	    return;
	}
	    	    
	//print
	Debug.println('+', "thread " + id + " created " + filename + ".");
    }
    
    /**
     * delete a file
     */
    private void deleteFile() {
	//choose between 0 and 1 again to decide what file to delete
	int choice = getRandom();
	filename = new String("file" + choice);
	    	    
	//create the file
	if (!Nachos.fileSystem.remove(filename)) {
	    Debug.println('+', "thread " + id + " could not delete " + filename + ".");
	    return;
	}
	
	//print results
	Debug.println('+', "thread " + id + " deleted " + filename + ".");
    }
    
    /**
     * write to the buffer a message
     */
    private void writeBuffer() {
	buffer[0] = 'l';
	buffer[1] = 'a';
	buffer[2] = 's';
	buffer[3] = 't';
	buffer[4] = 'w';
	buffer[5] = 'r';
	buffer[6] = 'o';
	buffer[7] = 't';
	buffer[8] = 'e';
	buffer[9] = (byte) id;
    }
    
    private String getChoiceString(int choice) {
	switch (choice) {
	case 0:
	    return "read";
	case 1:
	    return "write";
	case 2:
	    return "create";
	}
	return "delete";
    }
    
    /**
     * entry point for the test
     * @param threads the number of threads for the test to spawn
     */
    public static void start(int threads) {
	Debug.println('+', "Entering FileSystemStressTest");
	for (int i = 1;i <= threads;i++) {
	    new FileSystemStressTest(i, threads);
	}
    }
}
