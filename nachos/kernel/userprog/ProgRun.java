package nachos.kernel.userprog;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.machine.CPU;

/**
 * This is a class that loads programs into memory and executes them
 * 
 * @author Thomas Anderson (UC Berkeley), original C++ version
 * @author Peter Druschel (Rice University), Java translation
 * @author Eugene W. Stark (Stony Brook University)
 */
public class ProgRun implements Runnable {

    /** the space the process will work with */
    private AddrSpace space;
    
    /** the argument for the program if needed */
    private int arg;

    /**
     * Start the test by creating a new address space and user thread,
     * then arranging for the new thread to begin executing the run() method
     * of this class.
     *
     * @param filename The name of the program to execute.
     * @param space the address space the program will work with
     */
    public ProgRun(String filename, AddrSpace space) {
	String name = "ProgRun"+ "(" + filename + ")";
	this.space = space;
	
	Debug.println('+', "starting ProgRun: " + name);

	UserThread t = new UserThread(name, this, space);
	Nachos.scheduler.readyToRun(t);
    }
    
    /**
     * Start the test by creating a new address space and user thread,
     * then arranging for the new thread to begin executing the run() method
     * of this class.
     *
     * @param filename The name of the program to execute.
     * @param space the address space the program will work with
     * @param arg the initial argument for the program
     */
    public ProgRun(String filename, AddrSpace space, int arg) {
	String name = "ProgRun"+ "(" + filename + ")";
	this.space = space;
	this.arg = arg;
	
	Debug.println('+', "starting ProgRun: " + name);

	UserThread t = new UserThread(name, this, space);
	Nachos.scheduler.readyToRun(t);
    }

    /**
     * Entry point for the thread created to run the user program.
     * The specified executable file is used to initialize the address
     * space for the current thread.  Once this has been done,
     * CPU.run() is called to transfer control to user mode.
     */
    public void run() {

	space.initRegisters();		// set the initial register values
	space.restoreState();		// load page table register
	
	//if we have an initial argument, then write the value to register 4
	try {
	    CPU.writeRegister(4, arg);
	} catch (Exception e) {}

	CPU.runUserCode();		// jump to the user program
	Debug.ASSERT(false);		// machine->Run never returns;
	// the address space exits
	// by doing the syscall "exit"
    }

    /**
     * Entry point for ProgRun.
     */
    public static void start(String name, AddrSpace space) {
	Debug.ASSERT(Nachos.options.FILESYS_REAL || Nachos.options.FILESYS_STUB,
			"A filesystem is required to execute user programs");
	new ProgRun(name, space);
    }
    
    /**
     * Entry point for ProgRun.
     */
    public static void start(String name, AddrSpace space, int arg) {
	Debug.ASSERT(Nachos.options.FILESYS_REAL || Nachos.options.FILESYS_STUB,
			"A filesystem is required to execute user programs");
	new ProgRun(name, space, arg);
    }
}