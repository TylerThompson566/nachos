/* Basic test of malloc() and free() functions */

#include "syscall.h"

int
main()
{
	int *ptr = (int *) malloc(sizeof(int));
	*ptr = 1;
	Print(*ptr);
	free(ptr);
	Exit(0);
}
