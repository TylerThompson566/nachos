package nachos.kernel.threads.test;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.machine.NachosThread;
import nachos.machine.Simulation;

import java.util.Random;

/**
 * Simple test of the callout facility.
 * 
 * @author Eugene W. Stark
 * @version 20140301
 */
public class CalloutTest {
    
    private static final int NUM_THREADS = 5;
    
    // Use the RNG from Simulation in order to get repeatability.
    private static final Random random = Simulation.random;
    
    public static void start() {
	Debug.println('+', "Entering CalloutTest");
	for(int i = 0; i < NUM_THREADS; i++) {
	    final NachosThread t = new NachosThread("CalloutTest " + i);
	    t.setRunnable
	    	(new Runnable() {
	    	    public void run() {
	    		do {
	    		    int ticks = random.nextInt(10000);
	    		    Debug.println('+', "Thread " + t.name + " sleeping for "
	    			    	       + ticks + " ticks");
	    		    Nachos.scheduler.sleepThread(ticks);
	    		    Debug.println('+', "Thread " + t.name + " awake ");
	    		} while(random.nextInt(100) < 90);  // Flip a coin to decide if done
	    		Debug.println('+', "Thread " + t.name + " terminating");
	    		Nachos.scheduler.finishThread();
	    	    }
	    	});
	    Nachos.scheduler.readyToRun(t);
	}
    }

}
