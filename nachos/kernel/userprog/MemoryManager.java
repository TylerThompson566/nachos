package nachos.kernel.userprog;

import java.util.ArrayList;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.threads.Semaphore;
import nachos.machine.Machine;

/**
 * MemoryManager is a class that maintains a mapping of processes and what pages they're using.
 * The MemoryManager consists of an array of integers. Each index if the array is a page in main memory
 * and the integer stored in that index is mapped to the spaceID. The spaceID is the ID of the process
 * that is currently using that page in memory. A semaphore is used to ensure atomic access to the list.
 * @author Tyler
 *
 */
public class MemoryManager {
    
    //constants for identifying what type of page it is
    private final int CODE = 0;
    private final int DATA = 1;
    private final int STACK = 2;
    private final int EXTENDED_REGION = 3;
    
    /** The array of page-to-process mappings */
    private PageInfo[] coreMap;
    
    /** create a counter for the unique process identifiers */
    private int spaceIDCounter;
    
    /** The semaphore that ensures atomic access to the list */
    private Semaphore dirSem;
    
    /** The semaphore that ensures atomic access to the counter */
    private Semaphore counterSem;
    
    /** The list of ExitObjects */
    private ArrayList<ProcessObject> processList;
    
    /** the semaphore that ensures atomic access to processList */
    private Semaphore processListSem;
    
    /** the swap map that maps processes and VM to sector numbers on the paging/replacement disk */
    private SwapMap swapMap;
    
    /** the index of the last evicted page in physical memory */
    private int lastEvicted;
    
    /** Constructor for the memory manager */
    public MemoryManager() {
	
	//initialize the core map, the spaceIDCounter, and the semaphores
	coreMap = new PageInfo[Machine.NumPhysPages];
	dirSem = new Semaphore("MemoryManagerSem", 1);
	spaceIDCounter = 1;
	counterSem = new Semaphore("spaceIDCounterSem", 1);
	
	//initialize the ProcessList and semaphore
	processList = new ArrayList<ProcessObject>();
	processListSem = new Semaphore("ProcessListSemaphore", 1);
	
	//initialize the swap map
	swapMap = new SwapMap();
	lastEvicted = 0;
	
	//set up all of the page mappings
	for (int i = 0;i < coreMap.length;i++) {
	    coreMap[i] = null;
	}
    }
    
    /** get an unused spaceID to uniquely identify a process. increment the spaceIDCounter so it cannot be used anymore */
    public int getUnusedSpaceID() {
	
	//ensure we cannot go over the max value of space IDs
	Debug.ASSERT(spaceIDCounter != Integer.MAX_VALUE, "Error: ran out of space IDs");
	
	//create an ID that we can return
	int id;
	
	//safely get the ID and add the process to the list of IDs
	counterSem.P();
	id = spaceIDCounter;
	spaceIDCounter++;
	counterSem.V();
	
	//return the ID
	return id;
    }
    
    /** get an unused page and associate it with a process, returns -1 if error */
    public int allocatePage(int spaceID, int pageType) {
	
	//initialize the page we are returning to -1
	int page = -1;
		
	//atomically iterate through all of the pages and find an open one
	dirSem.P();
	for (int i = 0;i < coreMap.length;i++) {
		    
	//if we found an open page, set page to that index and exit the loop
	    if (coreMap[i] == null) {
		page = i;
		coreMap[i] = new PageInfo(spaceID, pageType);
		break;
	    }
	}
	
	//return access to the list
	dirSem.V();
		
	//return the page
	return page;
    }
    
    /** get the space ID associated with a page number */
    public int getPageSpaceID(int pageNumber) {
	dirSem.P();
	int id = coreMap[pageNumber].getSpaceID();
	dirSem.V();
	return id;
    }
    
    /** check to see if a page is free or not. */
    public boolean isUnused(int pageNumber) {
	boolean unused = false;
	dirSem.P();
	if (coreMap[pageNumber] == null) {
	    unused = true;
	}
	dirSem.V();
	return unused;
    }
    
    /** set all pages by a certain spaceID as unused */
    public void freeSpace(int spaceID) {
	
	//safely iterate through all of the pages and processes
	dirSem.P();
	for (int i = 0;i < coreMap.length;i++) {
	    
	    //if the page is being used by the argument spaceID, then set it to unused
	    if ((coreMap[i] != null) && (coreMap[i].getSpaceID() == spaceID)) {
		coreMap[i] = null;
	    }
	}
	
	//return access to the list
	dirSem.V();
    }
    
    /** free an individual page */
    public void freePage(int page) {
	
	//atomically set the spaceID of the page we wish to free to 0
	dirSem.P();
	coreMap[page] = null;
	dirSem.V();
    }
    
    /** add a process to the list of processes */
    public void addProcess(int spaceID) {
	
	//atomically add the process to the list, then return access to the list
	processListSem.P();
	processList.add(new ProcessObject(spaceID));
	processListSem.V();
	
    }
    
    /** join with the process that is used by spaceID */
    public void join(int spaceID) {
	
	//create a counter
	int i = 0;
	
	//loop infinitely
	while (true) {
	    
	    //atomically access the list
	    processListSem.P();
	    
	    //if we are outside of the list, then set the counter to 0
	    if (i >= processList.size()) {
		i = 0;
	    }
	    
	    //if we find the process we are looking for, then call P() on that ProcessObject's semaphore
	    else if (processList.get(i).getSpaceID() == spaceID) {
		
		//return access to the list before we wait
		processListSem.V();
		
		//wait until the process exits
		processList.get(i).processSem.P();

		//once the process exits, return to exit the loop
		return;
	    }
	    
	    //return access to the list
	    processListSem.V();
	    
	    //increment i
	    i++;
	}
    }
    
    /** let all join threads know that the process they want to join has exited */
    public void exit(int spaceID) {
	
	//atomically access the list
	processListSem.P();
	for (int i = 0;i < processList.size();i++) {
	    
	    //if we find our process, call V on its semaphore
	    if (processList.get(i).getSpaceID() == spaceID) {
		processList.get(i).processSem.V();
	    }
	}
	
	//return access to the list
	processListSem.V();
    }
    
    /**
     * Set all of the bytes in a page of main memory to zero
     * @param page the page in main memory being zeroed out
     */
    public void zeroOutPage(int page) {
	
	//iterate through the whole page
	for (int i = 0;i < Machine.PageSize;i++) {
	    
	    //zero out memory
	    Machine.mainMemory[(Machine.PageSize * page) + i] = (byte)0;
	}
    }
    
    /**
     * try to write the contents of the swap disk to physical memory if a mapping exists
     * @param spaceID the spaceID of the process that threw the page fault
     * @param VPN the virtual page number of the offending virtual address
     * @param PPN the physical page number of main memory we might need to write to
     */
    public void writeMapping(int spaceID, int VPN, int PPN) { 
	
	//get the sector on the swap map that has a mapping with the virtual page number and the spaceID
	int swapSector = swapMap.getSector(spaceID, VPN);
	
	//if the sector is -1, then there is no mapping. do nothing and return
	if (swapSector == -1) {
	    return;
	}
	
	if (Nachos.options.PAGING_TEST) {
	    System.out.println("Found swap map entry, writing to main memory from swap disk");
	}
	
	//create a buffer to store the data from the swap disk
	byte[] data = new byte[Nachos.pageDiskDriver.getSectorSize()];
	
	//we have the sector, we need to copy the data from that sector in the swap disk to main memory
	Nachos.pageDiskDriver.readSector(swapSector, data, 0);
	
	//copy the data from the swap disk to main memory
	writeData(data, PPN);
	
	//now that the data from the swap disk is now in main memory, we can remove the entry from the swap disk
	swapMap.removeEntry(spaceID, VPN, swapSector);
    }
    
    /**
     * write the contents of an array into main memory at a sector
     * @param data the buffer we are copying into main memory
     * @param page the page in physical memory we are writing to
     */
    private void writeData(byte[] data, int page) {
	
	//iterate through the whole page
	for (int i = 0;i < Machine.PageSize;i++) {
	    
	    //copy the memory
	    Machine.mainMemory[(Machine.PageSize * page) + i] = data[i];
	}
    }
    
    /**
     * read the contents of main memory and store it in a buffer
     * @param data the buffer we are copying into main memory
     * @param page the page in physical memory we are writing to
     */
    private byte[] readData(int page) {
	
	byte[] data = new byte[Machine.PageSize];
	
	//iterate through the whole page
	for (int i = 0;i < Machine.PageSize;i++) {
	    
	    //copy the memory
	    data[i] = Machine.mainMemory[(Machine.PageSize * page) + i];
	}
	
	return data;
    }
    
    /**
     * Evict a page that is being used as an extended region by another process, then allocate that page to the needing process
     * @param spaceID the spaceID of the process requesting the evicted page
     * @param pageType the type of page we are allocating
     * @return the page number of the evicted page we are allocating
     */
    public int allocateEvictedPage(int spaceID, int pageType) {
	
	//save the page number to an int and have its default value be error -1
	int page = -1;
	
	//increment lastEvicted by 1 so we do not evict the same page over and over again
	lastEvicted++;
	
	//iterate through all pages starting at the last evicted page (i is just a counter)
	for (int i = 0;i < coreMap.length;i++) {
	    
	    //if we go out of bounds, set last evicted page to 0
	    if (lastEvicted >= coreMap.length) {
		lastEvicted = 0;
	    }
	    
	    //if we find an appropriate page, allocate it and write the contents to the swap disk
	    //an appropriate page...
	    	//is not null (safety check)
	    	//is used for an extended region (do not overwrite code, data, or a stack)
	    	//does not belong to the process we are trying to get memory for
	    if ((coreMap[lastEvicted] != null) && 
		(coreMap[lastEvicted].getPageType() == EXTENDED_REGION) &&
		(coreMap[lastEvicted].getSpaceID() != spaceID)) {
		
		//write the data to the swap disk and record the mapping
		writeToSwapDisk(coreMap[lastEvicted].getSpaceID(), pageType, lastEvicted);
		
		//set the new allocation and set the page we are allocating to the page we found
		coreMap[lastEvicted] = new PageInfo(spaceID, pageType);
		page = lastEvicted;
		
		break;
	    }
	    
	    //get the next page to check if the last page was not appropriate for evicting
	    lastEvicted++;
	}
	
	//return the evicted page or -1 if error
	return page;
    }
    
    /**
     * write a page from main memory to the swap disk, then record it as a mapping
     * @param info the info on the page we are copying
     * @param page the page from main memory we are writing to the swap disk
     */
    private void writeToSwapDisk(int spaceID, int pageType, int page) {
	
	//create a place to store all data and copy main memory to the buffer
	byte[] data = readData(page);
	
	//find an unused sector in the swap disk
	for (int i = 0;i < Nachos.pageDiskDriver.getNumSectors();i++) {
	    
	    //if a sector is not used, then write the data to the sector we found, then create a mapping
	    if (!(swapMap.containsSector(i))) {
		Nachos.pageDiskDriver.writeSector(i, data, 0);
		swapMap.addEntry(spaceID, page, i);
		break;
	    }
	}
    }
    
    public int swapMapEntries() {
	return swapMap.size();
    }
}

/**
 * An object that represents a running process.
 * @param spaceID the ID that associates a process to the memory that it uses in main memory
 * @param processSem the semaphore that lets joining threads wait until this process exits
 * @author Tyler
 *
 */
class ProcessObject {
    
    //parameters
    private int spaceID;		//the spaceID of the process
    public Semaphore processSem;	//The semaphore that will be used for join and exit threads
    
    public ProcessObject(int spaceID) {
	
	//initialize spaceID
	this.spaceID = spaceID;
	
	//initialize the process semaphore to 0 so a joining thread has to wait until this process exits
	processSem = new Semaphore(("Process " + spaceID + " semaphore"), 0);
    }
    
    /** accessor for the spaceID of the process object */
    public int getSpaceID() {
	return spaceID;
    }
}

/**
 * the info about an allocated page
 * @author Tyler
 *
 */
class PageInfo { 
    
    /** the spaceID of the process this page belongs to */
    private int spaceID;
    
    /** the type of page */
    private int pageType;
    
    /** allocate a page and record it's info */
    public PageInfo(int spaceID, int pageType) {
	this.spaceID = spaceID;
	this.pageType = pageType;
    }
    
    /** get the space ID of the process associated with this page frame */
    public int getSpaceID() {
	return spaceID;
    }
    
    /** get the page type of the page frame */
    public int getPageType() {
	return pageType;
    }
}