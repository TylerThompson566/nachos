/* Basic test of malloc() and free() functions */

#include "syscall.h"

int
main()
{
	int *ptr = (int *) malloc(sizeof(int));
	int *ptr2 = (int *) malloc(sizeof(int));
	int *ptr3 = (int *) malloc(sizeof(int));
	*ptr = 1;
	*ptr2 = 2;
	*ptr3 = *ptr + *ptr2;
	Print(*ptr3);
	free(ptr);
	free(ptr2);
	free(ptr3);
	Exit(0);
}
