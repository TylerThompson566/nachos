/* Basic test of Yield() system call */

#include "syscall.h"

int
main()
{
	Exec("test/execTest");
	Yield();
	Exit(0);
}
