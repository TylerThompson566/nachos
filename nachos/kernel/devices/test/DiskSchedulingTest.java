package nachos.kernel.devices.test;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.devices.DiskRequest;
import nachos.kernel.threads.Semaphore;
import nachos.machine.NachosThread;
import nachos.util.DiskQueue;
import nachos.kernel.devices.DiskCSCANQueue;
import nachos.kernel.devices.DiskFCFSQueue;

/**
 * create a bunch of random disk requests, and test the validity of our scheduling methods
 * @author Tyler
 *
 */
public class DiskSchedulingTest implements Runnable {
    
    DiskQueue<DiskRequest> queue;

    public DiskSchedulingTest() {
	
	//initialize all of the requests
	if (Nachos.options.DISK_SCHEDULE_METHOD == Nachos.options.FCFS) {
	    queue = new DiskFCFSQueue<DiskRequest>();
	}
	else {
	    queue = new DiskCSCANQueue<DiskRequest>();
	}
	
	//initialize the thread and set it ready to run
	NachosThread t = new NachosThread("DiskSchedulingTest thread", this);
	Nachos.scheduler.readyToRun(t);
    }
    
    public void run() {
	
	//create a new, arbitrary disk request to start
	int sector = (int) (Math.random() * Nachos.diskDriver.getNumSectors());
	Nachos.diskDriver.setCurrentRequest(new DiskRequest(true, sector, new byte[Nachos.diskDriver.getSectorSize()], 0, new Semaphore("", 1)));
	
	//print the scheduling method and starting sector
	Debug.println('+', "Scheduling method: " + Nachos.diskDriver.getSchedulingMethod());
	Debug.println('+', "Starting sector: " + sector);
	
	//add the requests to the queue after letting us know what we added
	Debug.println('+', "Requests as added...");
	for (int i = 0;i < 10;i++) {
	    sector = (int) (Math.random() * Nachos.diskDriver.getNumSectors());
	    DiskRequest request = new DiskRequest(true, sector, new byte[Nachos.diskDriver.getSectorSize()], 0, new Semaphore("", 1));
	    Debug.println('+', "\tRequest " + (i + 1) + ": sector number = " + request.sectorNumber);
	    queue.offer(request);
	}
	
	//remove the request from the queue and let us know what we removed to see if we are removing the right request
	Debug.println('+', "Requests as removed from queue...");
	for (int i = 0;i < 10;i++) {
	    DiskRequest request = queue.poll();
	    Debug.println('+', "\tRequest " + (i + 1) + ": sector number = " + request.sectorNumber);
	    Nachos.diskDriver.setCurrentRequest(request);
	}
	
	//finish the thread
	Nachos.scheduler.finishThread();
    }
    
    public static void start() {
	Debug.println('+', "Entering DiskSchedulingTest");
	new DiskSchedulingTest();
    }
}
