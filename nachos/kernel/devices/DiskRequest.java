package nachos.kernel.devices;

import nachos.kernel.threads.Semaphore;

/**
 * A request put in by a thread to write or read data from the disk
 * @author Tyler
 *
 */
public class DiskRequest {
    
    /** true = read request,
     * false = write request */
    public boolean read;
    
    /** The sector the disk wishes to write/read to */
    public int sectorNumber;
    
    /** the data that is being written to the disk */
    public byte[] data;
    
    /** the index of the read/write request */
    public int index;
    
    /** the semaphore that is used to block the requesting thread */
    public Semaphore sem;
    
    /** create a disk request */
    public DiskRequest(boolean read, int sectorNumber, byte[] data, int index, Semaphore sem) {
	this.read = read;
	this.sectorNumber = sectorNumber;
	this.data = data;
	this.index = index;
	this.sem = sem;
    }
    
}
