package nachos.kernel.devices;

import java.util.Hashtable;

import nachos.Debug;
import nachos.kernel.threads.Semaphore;

/**
 * Manager for all file headers. File headers cannot have more than one instance of itself open at any time, so the manager ensures that threads waiting
 * to use a file header must wait until the thread currently using that file header is done using it so it can write back changes before letting another
 * thread read/write.
 * @author Tyler
 *
 */
public class FileHeaderManager {
    
    /** the table that contains a list of all of the semaphores associated with different file headers. the sector represents the sector where a file header is stored */
    Hashtable<Integer, Semaphore> headerTable;
    
    /** the semaphore that ensures atomic access to the table */
    private Semaphore tableSem;
    
    /** for testing, counts how many threads have finished the stress test */
    public int threadsFinished = 0;
    
    /** initialize the file header manager */
    public FileHeaderManager() {
	
	//initialize the objects
	headerTable = new Hashtable<Integer, Semaphore>();
	tableSem = new Semaphore("File Header Manager Semaphore", 1);
	
	//add the bit map and the directory sectors in the list from the start
	tableSem.P();
	headerTable.put(0, new Semaphore("Bit map header Semaphore", 1));
	headerTable.put(1, new Semaphore("Directory header Semaphore", 1));
	tableSem.V();
    }
    
    /**
     * call P on the semaphore associated with the sector of the file header we wish to access, and return once we have exclusive access to the file header
     * @param sector the sector where the file header we are waiting for is stored
     */
    public void waitForAccess(int sector) { 
	
	//gain atomic access to the list
	tableSem.P();
	
	//retrieve the semaphore associated with the file header
	Semaphore sem = headerTable.get(sector);
	
	//if we did not find the entry, add the entry
	if (sem == null) {
	    headerTable.put(sector, new Semaphore("File Header Semaphore (sector " + sector + ")", 1));
	    sem = headerTable.get(sector);
	}
	
	//return access to the table before waiting
	tableSem.V();
	
	//call P on the semaphore so this thread waits until no one is using this file header
	sem.P();
    }
    
    /**
     * call V on the semaphore associated with the sector of the file header we wish to relinquish access
     * @param sector the sector of the file header we are no longer using
     */
    public void returnAccess(int sector) {
	
	//gain atomic access to the list
	tableSem.P();
	
	//retrieve the semaphore, assert if we do not find it
	Semaphore sem = headerTable.get(sector);
	Debug.ASSERT(sem != null);
	
	//let the other threads know that we are no longer using this file header
	sem.V();
	
	//return access to the table
	tableSem.V();
    }
    
    /**
     * remove an entry from the manager as a result of a deletion or removal of a file
     * @param sector the sector where the file being removed is
     */
    public void removeEntry(int sector) {
	
	//gain atomic access to the list
	tableSem.P();
	
	//remove the entry
	headerTable.remove(sector);
	
	//return access to the table
	tableSem.V();
    }
    

}
