/* readTest.c
 *	Simple program to test whether the read syscall works or not
 */

#include "syscall.h"

int
main()
{
    char buffer[10];
    buffer[0] = 'h';
    buffer[1] = 'e';
    buffer[2] = 'l';
    buffer[3] = 'l';
    buffer[4] = 'o';
    buffer[5] = 'w';
    buffer[6] = 'o';
    buffer[7] = 'r';
    buffer[8] = 'l';
    buffer[9] = 'd';
    Write(buffer, 10, 1);
    
    Exit(0);
}