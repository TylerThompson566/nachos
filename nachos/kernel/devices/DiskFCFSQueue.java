package nachos.kernel.devices;

import nachos.util.DiskQueue;
import nachos.util.FIFOQueue;

/**
 * a queue that is first come first served, used by a DiskDriver to schedule its next read/write request
 * @author Tyler
 *
 * @param <T>
 */
public class DiskFCFSQueue<T> implements DiskQueue<T> {

    /** the queue of disk requests */
    FIFOQueue<DiskRequest> queue;
    
    /** initialize the queue */
    public DiskFCFSQueue() {
	queue = new FIFOQueue<DiskRequest>();
    }
    
    @Override
    public boolean offer(DiskRequest e) {
	return queue.offer(e);
    }

    @Override
    public DiskRequest peek() {
	return queue.peek();
    }

    @Override
    public DiskRequest poll() {
	return queue.poll();
    }

    @Override
    public boolean isEmpty() {
	return queue.isEmpty();
    }
    
}
