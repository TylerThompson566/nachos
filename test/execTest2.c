/* Basic test of Exec() system call */

#include "syscall.h"

int
main()
{
	PredictCPU(10);
	Exec("test/loop");
	Exec("test/loop2");
	PredictCPU(10);
	Exec("test/loop3");
	Exit(0);
}
