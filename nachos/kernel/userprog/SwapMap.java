package nachos.kernel.userprog;

import java.util.ArrayList;

import nachos.kernel.Nachos;
import nachos.kernel.threads.Semaphore;

/**
 * A map that maps processes and Virtual page numbers to sector numbers
 * on the paging/replacement disk.
 * @author Tyler
 *
 */
public class SwapMap {
    
    /** the hash table mapping pairs to sector numbers */
    ArrayList<SwapMapPairing> list;
    
    /** the semaphore that ensures atomic access to the table */
    Semaphore sem;
    
    /** initialize the swap map */
    public SwapMap() {
	list = new ArrayList<SwapMapPairing>();
	sem = new Semaphore("swap map semaphore", 1);
    }
    
    /** add an entry to the swap map */
    public void addEntry(int spaceID, int VPN, int sector) {
	
	//create a new pairing
	SwapMapPairing pairing = new SwapMapPairing(spaceID, VPN, sector);
	
	if (Nachos.options.PAGING_TEST) {
	    System.out.println("adding swap map entry\n\tspaceID = " + spaceID + "\n\tVPN = "+ VPN + "\n\tsector = " + sector);
	}
	
	//atomically add it to the list
	sem.P();
	list.add(pairing);
	sem.V();
    }
    
    /** remove an entry in the swap map */
    public void removeEntry(int spaceID, int VPN, int sector) {
	
	//atomically search the list and remove the value we need to
	sem.P();
	for (int i = 0;i < list.size();i++) {
	    if (list.get(i).equalsMapping(spaceID, VPN, sector)) {
		list.remove(i);
		break;
	    }
	}
	sem.V();
    }
    
    public int size() {
	return list.size();
    }
    
    /** see if the swap map contains a mapping */
    public boolean containsMapping(int spaceID, int VPN) {
	
	//atomically search the list
	sem.P();
	for (int i = 0;i < list.size();i++) {
	    if (list.get(i).equalsPairing(spaceID, VPN)) {
		sem.V();
		return true;
	    }
	}
	sem.V();
	
	return false;
    }
    
    /** see if the swap map contains a sector */
    public boolean containsSector(int sector) {
	
	//atomically search the list
	sem.P();
	for (int i = 0;i < list.size();i++) {
	    if (list.get(i).equalsSector(sector)) {
		sem.V();
		return true;
	    }
	}
	sem.V();
	
	return false;
    }
    
    /** get the sector on the swap disk that is associated with the parameters */
    public int getSector(int spaceID, int VPN) {
	
	//default error value
	int sector = -1;
	
	//atomically search the list
	sem.P();
	for (int i = 0;i < list.size();i++) {
	    if (list.get(i).equalsPairing(spaceID, VPN)) {
		sector = list.get(i).getSector();
		break;
	    }
	}
	sem.V();
	
	return sector;
    }
}

/**
 * an object that stores SpaceID and VPN pairs
 * @author Tyler
 *
 */
class SwapMapPairing {
    
    /** the spaceID of the process */
    private int spaceID;
    
    /** the virtual page number */
    private int VPN;
    
    /** the sector on the swap disk */
    private int sector;
    
    /** make a  */
    public SwapMapPairing(int spaceID, int VPN, int sector) {
	this.spaceID = spaceID;
	this.VPN = VPN;
	this.sector = sector;
    }
    
    /** get the spaceID of a pair */
    public int getSpaceID() {
	return spaceID;
    }
    
    /** get the virtual page number of a pair */
    public int getVPN() {
	return VPN;
    }
    
    /** get the sector on the swap disk */
    public int getSector() {
	return sector;
    }
    
    /** check if a mapping matches the parameters */
    public boolean equalsMapping(int spaceID, int VPN, int sector) {
	if (this.spaceID == spaceID &&
	    this.VPN == VPN &&
	    this.sector == sector) {
	    return true;
	    
	}
	return false;
    }
    
    /** check if a mapping matches the parameters (not including sector) */
    public boolean equalsPairing(int spaceID, int VPN) {
	if (this.spaceID == spaceID &&
	    this.VPN == VPN) {
	    return true;
	    
	}
	return false;
    }
    
    /** check if a mapping matches the sector */
    public boolean equalsSector(int sector) {
	if (this.sector == sector) {
	    return true;
	}
	return false;
    }
    
}
