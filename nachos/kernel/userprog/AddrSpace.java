// AddrSpace.java
//	Class to manage address spaces (executing user programs).
//
//	In order to run a user program, you must:
//
//	1. link with the -N -T 0 option 
//	2. run coff2noff to convert the object file to Nachos format
//		(Nachos object code format is essentially just a simpler
//		version of the UNIX executable object code format)
//	3. load the NOFF file into the Nachos file system
//		(if you haven't implemented the file system yet, you
//		don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// Copyright (c) 1998 Rice University.
// Copyright (c) 2003 State University of New York at Stony Brook.
// All rights reserved.  See the COPYRIGHT file for copyright notice and
// limitation of liability and disclaimer of warranty provisions.

package nachos.kernel.userprog;

import nachos.Debug;
import nachos.machine.CPU;
import nachos.machine.MIPS;
import nachos.machine.Machine;
import nachos.machine.TranslationEntry;
import nachos.noff.NoffHeader;
import nachos.kernel.Nachos;
import nachos.kernel.filesys.OpenFile;

/**
 * This class manages "address spaces", which are the contexts in which
 * user programs execute.  For now, an address space contains a
 * "segment descriptor", which describes the the virtual-to-physical
 * address mapping that is to be used when the user program is executing.
 * As you implement more of Nachos, it will probably be necessary to add
 * other fields to this class to keep track of things like open files,
 * network connections, etc., in use by a user program.
 *
 * NOTE: Most of what is in currently this class assumes that just one user
 * program at a time will be executing.  You will have to rewrite this
 * code so that it is suitable for multiprogramming.
 * 
 * @author Thomas Anderson (UC Berkeley), original C++ version
 * @author Peter Druschel (Rice University), Java translation
 * @author Eugene W. Stark (Stony Brook University)
 */
public class AddrSpace {
    
    //constants for identifying what type of page it is
    private final int CODE = 0;
    private final int DATA = 1;
    private final int STACK = 2;
    private final int EXTENDED_REGION = 3;

    /** Page table that describes a virtual-to-physical address mapping. */
    private TranslationEntry pageTable[];

    /** Default size of the user stack area -- increase this as necessary! */
    private static final int UserStackSize = 1024;
    
    /** The identifier for the process being stored in main memory */
    private int spaceID;

    /**
     * Create a new address space.
     */
    public AddrSpace() { }
    
    /**
     * Create a copy of another address space
     */
    public AddrSpace(AddrSpace oldSpace) {
	pageTable = oldSpace.getPageTable();
	spaceID = oldSpace.getSpaceID();
    }
    
    /**
     * Get the page table of the space
     */
    public TranslationEntry[] getPageTable() {
	return pageTable;
    }
    
    /**
     * Get the spaceID of the process
     */
    public int getSpaceID() {
	return spaceID;
    }
    
    /**
     * Load the program from a file "executable", and set everything
     * up so that we can start executing user instructions.
     *
     * Assumes that the object code file is in NOFF format.
     *
     * First, set up the translation from program memory to physical 
     * memory.
     *
     * @param executable The file containing the object gitcode to 
     * 	load into memory
     * @param spaceID the identifier for the process being written to main memory
     * @return -1 if an error occurs while reading the object file,
     *    otherwise 0.
     */
    public int exec(OpenFile executable) {
	NoffHeader noffH;
	long size;
	
	if((noffH = NoffHeader.readHeader(executable)) == null)
	    return(-1);

	// how big is address space?
	size = roundToPage(noffH.code.size)
	     + roundToPage(noffH.initData.size + noffH.uninitData.size)
	     + roundToPage(UserStackSize);	// we need to increase the size
    						// to leave room for the stack
	
	//calculate how many pages we have to map
	int numPages = (int)(size / Machine.PageSize);
	
	//find out how many pages we need for each section
	int numCodePages = getNumPages(noffH.code.size);
	int numInitPages = getNumPages(noffH.initData.size);
	int numUninitPages = getNumPages(noffH.uninitData.size);
	int numDataPages = getNumPages(noffH.initData.size + noffH.uninitData.size);

	// check we're not trying to run anything too big
	// at least until we have virtual memory
	Debug.ASSERT((numPages <= Machine.NumPhysPages),
		 "AddrSpace constructor: Not enough memory!");

	Debug.println('a', "Initializing address space, numPages=" 
		+ numPages + ", size=" + size);
	
	//get the space ID for the process
	spaceID = Nachos.memoryManager.getUnusedSpaceID();

	// first, set up the translation 
	pageTable = new TranslationEntry[numPages];
	
	//create a counter for the overall page
	int pageCounter = 0;
	
	//keep track of the highest page
	int highestPage = 0;
	
	//set up code translations
	for (int i = 0;i < numCodePages;i++) {
	    
	    //create a new entry
	    pageTable[pageCounter] = new TranslationEntry();
	    
	    //set up all attributes for the page
	    pageTable[pageCounter].virtualPage = (noffH.code.virtualAddr + (i * Machine.PageSize)) / Machine.PageSize;
	    pageTable[pageCounter].valid = true;
	    pageTable[pageCounter].use = false;
	    pageTable[pageCounter].dirty = false;
	    pageTable[pageCounter].readOnly = true;	//code is read only
	    pageTable[pageCounter].physicalPage = Nachos.memoryManager.allocatePage(spaceID, CODE);
	    
	    //record the highest virtual page
	    if (pageTable[pageCounter].virtualPage > highestPage) {
		highestPage = pageTable[pageCounter].virtualPage;
	    }
	    
	    //increment the pageCounter
	    pageCounter++;
	    
	    
	}
	
	//set up initialized data translations
	for (int i = 0;i < numDataPages;i++) {
	    
	    //create a new entry
	    pageTable[pageCounter] = new TranslationEntry();
	    
	    //set up all attributes for the page
	    pageTable[pageCounter].virtualPage = (noffH.initData.virtualAddr + (i * Machine.PageSize)) / Machine.PageSize;
	    pageTable[pageCounter].valid = true;
	    pageTable[pageCounter].use = false;
	    pageTable[pageCounter].dirty = false;
	    pageTable[pageCounter].readOnly = false;
	    pageTable[pageCounter].physicalPage = Nachos.memoryManager.allocatePage(spaceID, DATA);
	    
	    //record the highest virtual page
	    if (pageTable[pageCounter].virtualPage > highestPage) {
		highestPage = pageTable[pageCounter].virtualPage;
	    }
	    
	    //increment the pageCounter
	    pageCounter++;
	}
	
	/*
	//set up uninitialized data translations
	for (int i = 0;i < numUninitPages;i++) {
	    
	    //create a new entry
	    pageTable[pageCounter] = new TranslationEntry();
	    
	    //set up all attributes for the page
	    pageTable[pageCounter].virtualPage = (noffH.uninitData.virtualAddr + (i * Machine.PageSize));
	    pageTable[pageCounter].valid = true;
	    pageTable[pageCounter].use = false;
	    pageTable[pageCounter].dirty = false;
	    pageTable[pageCounter].readOnly = false;
	    pageTable[pageCounter].physicalPage = Nachos.memoryManager.allocatePage(spaceID);
	    
	    //increment the pageCounter
	    pageCounter++;
	}
	*/
	
	//set up stack translations
	for (int i = 0;(i < getNumPages(UserStackSize))/* && (pageCounter < pageTable.length)*/;i++) {
	    
	    //create a new entry
	    pageTable[pageCounter] = new TranslationEntry();
	    
	    //set up all attributes for the page
	    pageTable[pageCounter].virtualPage = highestPage + 1;
	    pageTable[pageCounter].valid = true;
	    pageTable[pageCounter].use = false;
	    pageTable[pageCounter].dirty = false;
	    pageTable[pageCounter].readOnly = false;
	    pageTable[pageCounter].physicalPage = Nachos.memoryManager.allocatePage(spaceID, STACK);
	    
	    //record the highest virtual page
	    if (pageTable[pageCounter].virtualPage > highestPage) {
		highestPage = pageTable[pageCounter].virtualPage;
	    }
	    
	    //increment the pageCounter
	    pageCounter++;
	}
	
	// Zero out the entire address space, to zero the uninitialized data 
	// segment and the stack segment.
	for (int i = 0;i < pageTable.length;i++) {
	    Nachos.memoryManager.zeroOutPage(pageTable[i].physicalPage);
	}
	
	if (Nachos.options.PRINT_ADDRSPACE_INFO) {
	    System.out.println("\nPAGE MAPPING TEST - SpaceID = " + spaceID);
	    System.out.println("PAGE REQUIREMENTS");
	    System.out.println("\tTotal - " + numPages);
	    System.out.println("\tCode - " + numCodePages);
	    System.out.println("\tData - " + (numInitPages + numUninitPages) + " (" + numInitPages + " init, " + numUninitPages + " uninit)");
	    System.out.println("\tStack - " + (UserStackSize / Machine.PageSize));
	    System.out.println("-----------------------------------------------------------------------");
	    for (int i = 0;i < numPages;i++) {
		System.out.println("\tPageTable[" + i + "].virtualPage = " + pageTable[i].virtualPage);
		System.out.println("\tPageTable[" + i + "].physicalPage = " + pageTable[i].physicalPage);
		System.out.println("\tMemoryManager[" + pageTable[i].physicalPage + "] spaceID: " + Nachos.memoryManager.getPageSpaceID(pageTable[i].physicalPage) + "\n");
	    }
	    System.out.println("-----------------------------------------------------------------------\n");
	
	
	
	    System.out.println("\nZEROING TEST");
	    System.out.println("-----------------------------------------------------------------------");
	    for (int i = 0;i < Machine.NumPhysPages;i++) {
		System.out.print("PAGE " + i);
		if (Nachos.memoryManager.getPageSpaceID(i) == spaceID) {
		    System.out.print(" (IN USE)");
		}
		System.out.println();
		for (int k = 0;k < Machine.PageSize;k++) {
		    System.out.print(Machine.mainMemory[k + (i * Machine.NumPhysPages)]);
		}
		System.out.println();
	    }
	    System.out.println("-----------------------------------------------------------------------\n");
	}
	
	//write the code and the data to memory
	writeCode(executable, noffH, numCodePages);
	writeData(executable, noffH, numPages);
	
	if (Nachos.options.PRINT_ADDRSPACE_INFO) {
	    System.out.println("\nREADING TEST");
	    System.out.println("-----------------------------------------------------------------------");
	    for (int j = 0;j < Machine.NumPhysPages;j++) {
		System.out.print("PAGE " + j);
		if (!Nachos.memoryManager.isUnused(j)) {
		    System.out.print(" (IN USE)");
		}
		System.out.println();
		for (int k = 0;k < Machine.PageSize;k++) {
		    System.out.print("|" + Machine.mainMemory[k + (j * Machine.NumPhysPages)]);
		}
		System.out.println();
	    }
	    System.out.println("-----------------------------------------------------------------------\n");
	    
	    System.out.println("\nPAGE TABLE PRINT");
	    System.out.println("-----------------------------------------------------------------------");
	    for (int i = 0;i < pageTable.length;i++) {
		System.out.println("pageTable[" + i + "].virtualPage = " + pageTable[i].virtualPage);
	    }
	    System.out.println("-----------------------------------------------------------------------\n");
	}

	return(0);
    }
    
    /**
     * Allocate main memory so that the calling space has a new stack. Used in fork()
     * @return 0 if successful, -1 if error
     */
    public int allocNewStack() {
	
	//get the number of pages
	int numStackPages = getNumPages(UserStackSize);
	
	//keep an array of pages we have allocated and set all of the pages to -2
	int[] pagesAllocated = new int[numStackPages];
	for (int i = 0;i < pagesAllocated.length;i++) {
	    pagesAllocated[i] = -2;
	}
	
	//since the stack is in the last part of the page table, iterate through the last parts of the table
	for (int i = (pageTable.length - 1);i > ((pageTable.length - 1) - numStackPages);i--) {
	    
	    //allocate a page for part of the stack
	    pageTable[i].virtualPage = 0;
	    pageTable[i].valid = true;
	    pageTable[i].use = false;
	    pageTable[i].dirty = false;
	    pageTable[i].readOnly = false;
	    pageTable[i].physicalPage = Nachos.memoryManager.allocatePage(spaceID, STACK);
	    
	    //if we could not allocated the page, return all of the pages we allocated and return error code
	    if (pageTable[i].physicalPage == -1) {
		
		//iterate through all of the pages we allocated
		for (int k = 0;k < pagesAllocated.length;k++) {
		    
		    //if the page number in the array is not the default value, then we allocated it and we must free it
		    if (pagesAllocated[i] != -2) {
			Nachos.memoryManager.freePage(pagesAllocated[i]);
		    }
		}
		
		//return the error code
		return -1;
	    }
	    
	    //add the page to the list of pages we have allocated
	    for (int k = 0;k < pagesAllocated.length;k++) {
		if (pagesAllocated[k] != -2) {
		    pagesAllocated[k] = pageTable[i].physicalPage;
		}
	    }
	}
	
	//return normal code if we reached this far, we finished allocating a new stack for the forked program
	return 0;
    }

    /**
     * Initialize the user-level register set to values appropriate for
     * starting execution of a user program loaded in this address space.
     *
     * We write these directly into the "machine" registers, so
     * that we can immediately jump to user code.
     */
    public void initRegisters() {
    
    
	int i;
	for (i = 0; i < MIPS.NumTotalRegs; i++)
	    CPU.writeRegister(i, 0);

	// Initial program counter -- must be location of "Start"
	CPU.writeRegister(MIPS.PCReg, 0);	

	// Need to also tell MIPS where next instruction is, because
	// of branch delay possibility
	CPU.writeRegister(MIPS.NextPCReg, 4);

	// Set the stack register to the end of the segment.
	// NOTE: Nachos traditionally subtracted 16 bytes here,
	// but that turns out to be to accomodate compiler convention that
	// assumes space in the current frame to save four argument registers.
	// That code rightly belongs in start.s and has been moved there.
	int sp = pageTable.length * Machine.PageSize;
	CPU.writeRegister(MIPS.StackReg, sp);
	Debug.println('a', "Initializing stack register to " + sp);
    }

    /**
     * On a context switch, save any machine state, specific
     * to this address space, that needs saving.
     *
     * For now, nothing!
     */
    public void saveState() {}

    /**
     * On a context switch, restore any machine state specific
     * to this address space.
     *
     * For now, just tell the machine where to find the page table.
     */
    public void restoreState() {
	CPU.setPageTable(pageTable);
    }

    /**
     * Utility method for rounding up to a multiple of CPU.PageSize;
     */
    private long roundToPage(long size) {
	return(Machine.PageSize * ((size+(Machine.PageSize-1))/Machine.PageSize));
    }
    
    /**
     *  get the number of pages required for a number of bytes
     */
    private int getNumPages(int bytes) {
	
	if (bytes == 0) {
	    return 0;
	}
	return (int) (roundToPage(bytes) / Machine.PageSize);
    }
    
    /**
     *  write the code section to memory 
     */
    private void writeCode(OpenFile executable, NoffHeader noffH, int numCodePages) {
	
	//keep track of how many bytes we have written to memory, at first it is all of the code
	int bytesRemaining = noffH.code.size;
	
	//just return if there is nothing to write
	if (noffH.code.size == 0) {
	    return;
	}
	
	//write to all of the code pages
	for (int i = 0;i < numCodePages;i++) {
	    
	    //seek the place we want to read from in the file
	    executable.seek(noffH.code.inFileAddr + (i * Machine.PageSize));
	    
	    //if the remaining amount of data is less then a page, then write all you can
	    if (bytesRemaining < Machine.PageSize) {
		
		//read the file data into memory
		executable.read(Machine.mainMemory,
				  (pageTable[i].physicalPage * Machine.PageSize),
				  bytesRemaining);
		
		//we read all of the remaining bytes, so there are 0 bytes left to read
		bytesRemaining = 0;
	    }
	    
	    //otherwise, read in a page's worth of data
	    else {
		//read the file data into memory
		executable.read(Machine.mainMemory,					//read to main memory
				(pageTable[i].physicalPage * Machine.PageSize),		//read to the i'th page in memory
				Machine.PageSize);					//read in a page's worth of data
				
		//we read a page's worth of data from the file
		bytesRemaining -= Machine.PageSize;
	    }
	}
    }
    
    /** 
     *  write the data section to memory
     */
    private void writeData(OpenFile executable, NoffHeader noffH, int numPages) {
	
	//keep track of how many bytes we have written to memory, at first it is all of the initialized data
	//we do not write uninitialized data to memory
	int bytesRemaining = noffH.initData.size;
		
	//just return if there is nothing to write
	if (noffH.initData.size == 0) {
	    return;
	}
	
	//create a seek counter so we can only move up where we are looking in the file when we find a page we are writing to
	int seekCounter = 0;
		
	//iterate through all of the pages looking for a page that wasn't written to yet
	for (int i = 0;i < numPages;i++) {
	    
	    //if the page is read-only, then do not write to it
	    if (pageTable[i].readOnly) {
		//do nothing, we are not writing to it
	    }
	    
	    //if the page is not read-only, then we will write to it
	    else {
		
		//seek the place we want to read from in the file
		executable.seek(noffH.initData.inFileAddr + (seekCounter * Machine.PageSize));
		
		//if the remaining amount of data is less then a page, then write all you can
		if (bytesRemaining < Machine.PageSize) {
				
			//read the file data into memory
			executable.read(Machine.mainMemory,					//read to main memory
					(pageTable[i].physicalPage * Machine.PageSize),	//read to the i'th page in memory
					bytesRemaining);					//read in the remaining bytes of the code
				
			//we read all of the remaining bytes, so there are 0 bytes left to read
			bytesRemaining = 0;
		}
		
		//otherwise, read in a page's worth of data
		else {
		    //read the file data into memory
		    executable.read(Machine.mainMemory,					//read to main memory
			    	    (pageTable[i].physicalPage * Machine.PageSize),	//read to the i'th page in memory
				    Machine.PageSize);					//read in a page's worth of data
						
		    //we read a page's worth of data from the file
		    bytesRemaining -= Machine.PageSize;
		}
		
		//since we read, we need to move the seekCounter up so we can read the next chunk of data
		seekCounter++;
	    }
	}
    }
    
    /**
     * Create a copy of another address space with an increased size
     */
    public void handleAddressErrorException() {

	//get the bad address
	int badAddress = CPU.readRegister(MIPS.BadVAddrReg);
	if (Nachos.options.PRINT_ADDRSPACE_INFO) {
	    System.out.println("Bad Virtual address accessed = " + badAddress);
	}
	
	//get the pages needed based off of the bad address accessed. we need to cover it so we can access it
	int pagesNeeded = getNumPages((int) roundToPage(badAddress)) + 1;
	if (Nachos.options.PRINT_ADDRSPACE_INFO) {
	    System.out.println("number of pages in old page table = " + pageTable.length);
	    System.out.println("Pages needed = " + pagesNeeded);
	}
	
	//find the highest virtual page number so we can find the virtual addresses after the last virtual address in the 
	//old page table
	int highestPage = 0;
	for (int i = 0;i < pageTable.length;i++) {
	    if (pageTable[i].virtualPage > highestPage) {
		highestPage = pageTable[i].virtualPage;
	    }
	}
	if (Nachos.options.PRINT_ADDRSPACE_INFO) {
	    System.out.println("Highest page = " + highestPage);
	}

	//create a new page table with the necessary pages
	TranslationEntry[] newTable = new TranslationEntry[pagesNeeded];
	
	//fill up the new page table with the old translation entries
	int counter;
	for (counter = 0;counter < pageTable.length;counter++) {
	    newTable[counter] = new TranslationEntry();
	    newTable[counter].virtualPage = pageTable[counter].virtualPage;
	    newTable[counter].valid = pageTable[counter].valid;
	    newTable[counter].use = pageTable[counter].use;
	    newTable[counter].dirty = pageTable[counter].dirty;
	    newTable[counter].readOnly = pageTable[counter].readOnly;
	    newTable[counter].physicalPage = pageTable[counter].physicalPage;
	}
	
	//mark the rest of the entries as invalid and do not allocate any memory
	for (int i = 1;counter < newTable.length;counter++) {
	    newTable[counter] = new TranslationEntry();
	    newTable[counter].virtualPage = highestPage + i;			//the pages after the highest page
	    newTable[counter].valid = false;					//mark as invalid
	    newTable[counter].use = false;					//not in use
	    newTable[counter].dirty = false;					//not dirty
	    newTable[counter].readOnly = false;					//not read-only, that's only for code
	    newTable[counter].physicalPage = 0;					//do not allocate any memory
	    i++;								//increment i after every loop to get the next page
	}
	
	//we have set up the new page table, replace them
	pageTable = newTable;
	
	//set the new page table for the CPU
	CPU.setPageTable(pageTable);
	
    }
    
    /**
     * handle a page fault exception by finding the page accessed, and allocating memory for it
     */
    public void handlePageFaultException() {
	
	//get the failed virtual address from the MIPS register
	int addressAccessed = CPU.readRegister(MIPS.BadVAddrReg);
	
	if (Nachos.options.PRINT_ADDRSPACE_INFO) {
	    System.out.println("Bad Virtual address accessed = " + addressAccessed);
	}
	
	//find the page where this virtual address lies
	int pageAccessed = addressAccessed / Machine.PageSize;
	
	//find the table entry that has the page accessed
	int pageTableEntry = -1;
	for (int i = 0;i < pageTable.length;i++) {
	    if (pageTable[i].virtualPage == pageAccessed) {
		pageTableEntry = i;
	    }
	}
	
	//ensure we cannot pass a table entry with a negative value
	Debug.ASSERT((pageTableEntry >= 0), "Error: could not find the table entry containing the page accessed erroneously");
	
	//allocate a physical page for the virtual memory page allocated
	pageTable[pageTableEntry].physicalPage = Nachos.memoryManager.allocatePage(spaceID, EXTENDED_REGION);
	
	//if we cannot allocate any more space, evict a page
	if (pageTable[pageTableEntry].physicalPage < 0) {
	    if (Nachos.options.PAGING_TEST) {
		System.out.println("EVICTING PAGE");
	    }
	    pageTable[pageTableEntry].physicalPage = Nachos.memoryManager.allocateEvictedPage(spaceID, EXTENDED_REGION);
	}
	
	//assert if we cannot allocate any more space
	Debug.ASSERT((pageTable[pageTableEntry].physicalPage >= 0), "Error: could not allocate space");
	
	//zero out the page we are trying to access
	Nachos.memoryManager.zeroOutPage(pageTable[pageTableEntry].physicalPage);
	
	//try to write contents of the swap disk if a mapping exists
	Nachos.memoryManager.writeMapping(spaceID, pageTable[pageTableEntry].virtualPage, pageTable[pageTableEntry].physicalPage);

	//set the page entry to be valid
	pageTable[pageTableEntry].valid = true;
	
	//set the new page table for the CPU
	CPU.setPageTable(pageTable);
    }
    
    /**
     * initialize the space for doing the paging test
     */
    public void initPagingTest() {
	
	//set the spaceID of this process to 2, so we can use 1 to fill up the page table to not run into conflicts
	spaceID = 2;
	
	//create a new page table that will take up all of physical memory
	pageTable = new TranslationEntry[Machine.NumPhysPages];
	
	//allocate all of main memory
	for (int i = 0;i < pageTable.length;i++) {
	    
	    //get an allocated page
	    int allocatedPage = Nachos.memoryManager.allocatePage(1, EXTENDED_REGION);
	    
	    //if we couldn't allocate a page, exit the loop
	    if (allocatedPage == -1) {
		break;
	    }
	    
	    //otherwise, act as if we got an address error exception and fill the pages with invalid entries
	    else {
		pageTable[i] = new TranslationEntry();
		pageTable[i].virtualPage = i;
		pageTable[i].valid = false;
		pageTable[i].use = false;
		pageTable[i].dirty = false;
		pageTable[i].readOnly = false;
		pageTable[i].physicalPage = allocatedPage;
	    }
	}
	
	//print out all page table entries
	for (int i = 0;i < pageTable.length;i++) {
	    System.out.println("pageTable[" + i + "].virtualPage = " + pageTable[i].virtualPage);
	    System.out.println("pageTable[" + i + "].physicalPage = " + pageTable[i].physicalPage);
	}
	System.out.println("");
	
	//simulate a page fault error with all of main memory allocated
	CPU.writeRegister(MIPS.BadVAddrReg, ((Machine.PageSize * Machine.NumPhysPages) - 2));
	System.out.println("Swap map contains " + Nachos.memoryManager.swapMapEntries() + " entries before page fault");
	
	handlePageFaultException();
	System.out.println("Physical page to spaceID mappings");
	for (int i = 0;i < Machine.NumPhysPages;i++) {
	    System.out.println("physical page " + i + ": " + Nachos.memoryManager.getPageSpaceID(i));
	}
	System.out.println("");
	System.out.println("Swap map contains " + Nachos.memoryManager.swapMapEntries() + " entries after page fault");
	
	//test writing back to main memory from the swap disk
	//free the last page, and write the contents of the swap disk according to the mapping to a free block
	Nachos.memoryManager.freePage(15);
	CPU.writeRegister(MIPS.BadVAddrReg, Machine.PageSize - 20);
	System.out.println("Swap map contains " + Nachos.memoryManager.swapMapEntries() + " entries before page fault");
	
	handlePageFaultException();
	for (int i = 0;i < Machine.NumPhysPages;i++) {
	    System.out.println("physical page " + i + ": " + Nachos.memoryManager.getPageSpaceID(i));
	}
	System.out.println("");
	System.out.println("Swap map contains " + Nachos.memoryManager.swapMapEntries() + " entries after page fault");
	
	
    }
}