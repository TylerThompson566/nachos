// Directory.java
//	Class to manage a directory of file names.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// Copyright (c) 1998 Rice University.
// Copyright (c) 2003 State University of New York at Stony Brook.
// All rights reserved.  See the COPYRIGHT file for copyright notice and 
// limitation of liability and disclaimer of warranty provisions.

package nachos.kernel.filesys;

import java.util.ArrayList;

import nachos.Debug;
import nachos.kernel.Nachos;

/**
 * This class class defines a UNIX-like "directory".  Each entry in
 * the directory describes a file, and where to find it on disk.
 *
 * The directory is a table of fixed length entries; each
 * entry represents a single file, and contains the file name,
 * and the location of the file header on disk.  The fixed size
 * of each directory entry means that we have the restriction
 * of a fixed maximum size for file names.
 *
 * Also, this implementation has the restriction that the size
 * of the directory cannot expand.  In other words, once all the
 * entries in the directory are used, no more files can be created.
 * Fixing this is one of the parts to the assignment.
 *
 * The directory data structure can be stored in memory, or on disk.
 * When it is on disk, it is stored as a regular Nachos file.
 * The constructor initializes a directory structure in memory; the
 * fetchFrom/writeBack operations shuffle the directory information
 * from/to disk. 
 * 
 * We assume mutual exclusion is provided by the caller.
 * 
 * @author Thomas Anderson (UC Berkeley), original C++ version
 * @author Peter Druschel (Rice University), Java translation
 * @author Eugene W. Stark (Stony Brook University)
 */
class Directory {

    /** Number of entries in the directory. */
    private int tableSize;

    /** Table of pairs: file name/file header location. */
    private DirectoryEntry table[];

    /** The underlying filesystem in which the directory resides. */
    private final FileSystemReal filesystem;

    /**
     * Initialize a directory; initially, the directory is completely
     * empty.  If the disk is being formatted, an empty directory
     * is all we need, but otherwise, we need to call FetchFrom in order
     * to initialize it from disk.
     *
     * @param size The number of entries in the directory.
     * @param filesystem  The underlying filesystem in which this directory exists.
     */
    Directory(int size, FileSystemReal filesystem)
    {
	this.filesystem = filesystem;
	table = new DirectoryEntry[size];
	tableSize = size;
	for (int i = 0; i < tableSize; i++) {
	    table[i] = new DirectoryEntry();
	}
    }

    /**
     * Read the contents of the directory from disk.
     *
     * @param file The file containing the directory contents.
     */
    void fetchFrom(OpenFile file) {
	byte buffer[] = new byte[tableSize * DirectoryEntry.sizeOf()];
	file.readAt(buffer, 0, tableSize * DirectoryEntry.sizeOf(), 0);
	int pos = 0;
	for (int i = 0; i < tableSize; i++) {
	    table[i].internalize(buffer, pos);
	    pos += DirectoryEntry.sizeOf();
	}
    }

    /**
     * Write any modifications to the directory back to disk
     *
     * @param file The file to contain the new directory contents.
     */
    void writeBack(OpenFile file) {
	byte buffer[] = new byte[tableSize * DirectoryEntry.sizeOf()];
	int pos = 0;
	for (int i = 0; i < tableSize; i++) {
	    table[i].externalize(buffer, pos);
	    pos += DirectoryEntry.sizeOf();
	}
	file.writeAt(buffer, 0, tableSize * DirectoryEntry.sizeOf(), 0);
    }

    /**
     * Look up file name in directory, and return its location in the table of
     * directory entries.  Return -1 if the name isn't in the directory.
     *
     * @param name The file name to look up.
     * @return The index of the entry in the table, if present, otherwise -1.
     */
    private int findIndex(String name) {
	for (int i = 0; i < tableSize; i++) {
	    if (table[i].inUse() && name.equals(table[i].getName()))
		return i;
	}
	return -1;		// name not in directory
    }

    /**
     * Look up file name in directory, and return the disk sector number
     * where the file's header is stored. Return -1 if the name isn't 
     * in the directory.
     *
     * @param name The file name to look up.
     * @return The disk sector number where the file's header is stored,
     * if the entry was found, otherwise -1.
     */
    int find(String name) {
	int i = findIndex(name);

	if (i != -1)
	    return table[i].getSector();
	return -1;
    }

    /**
     * Add a file into the directory.  Return TRUE if successful;
     * return FALSE if the file name is already in the directory,
     * or if the directory is completely full, and has no more space for
     * additional file names, or if the file name cannot be represented
     * in the number of bytes available in a directory entry.
     *
     * @param name The name of the file being added.
     * @param newSector The disk sector containing the added file's header.
     * @return true if the file was successfully added, otherwise false.
     */
    boolean add(String name, int newSector) { 
	if (findIndex(name) != -1)
	    return false;

	for (int i = 0; i < tableSize; i++)
	    if (!table[i].inUse()) {
		if(!table[i].setUsed(name, newSector))
		    return(false);
		return(true);
	    }
	return false;	// no space.  Fix when we have extensible files.
    }

    /**
     * Remove a file name from the directory.  Return TRUE if successful;
     * return FALSE if the file isn't in the directory. 
     *
     * @param name The file name to be removed.
     */
    boolean remove(String name) { 
	int i = findIndex(name);

	if (i == -1)
	    return false; 		// name not in directory
	table[i].setUnused();
	return true;	
    }

    /**
     * List all the file names in the directory (for debugging).
     */
    void list() {
	for (int i = 0; i < tableSize; i++)
	    if (table[i].inUse())
		System.out.println(table[i].getName());
    }

    /**
     * List all the file names in the directory, their FileHeader locations,
     * and the contents of each file (for debugging).
     */
    void print() {
	FileHeader hdr = new FileHeader(filesystem);

	System.out.print("Directory contents: ");
	for (int i = 0; i < tableSize; i++)
	    if (table[i].inUse()) {
		System.out.println("Name " + table[i].getName()
			+ ", Sector: " + table[i].getSector());
		hdr.fetchFrom(table[i].getSector());
		hdr.print();
	    }
	System.out.println("");
    }
    
    
    /**
     * get all of the used sectors and place them in an array list
     */
    public int[] getUsedSectors() {
	
	//create a new list and a file header
	ArrayList<Integer> list = new ArrayList<Integer>();
	FileHeader header = new FileHeader((FileSystemReal) Nachos.fileSystem);
	
	//iterate through the whole table looking for files
	for (int i = 0;i < tableSize;i++) {
	    
	    //if we find a file, add all of the sectors it uses to the list
	    if (table[i].inUse()) {
		list.add(new Integer(table[i].getSector()));
		int sector = table[i].getSector();
		header.fetchFrom(sector);
		int[] fileSectors = header.getSectors();
		for (int k = 0;k < fileSectors.length;k++) {
		    if (fileSectors[k] >= 0) {
			list.add(new Integer(fileSectors[k]));
		    }
		}
	    }
	}
	
	//put all of the list values into an array
	int[] sectors = new int[list.size()];
	for (int i = 0;i < list.size();i++) { 
	    sectors[i] = list.get(i).intValue();
	}
	
	//return the array
	return sectors;
    }
    
    /**
     * check the directory to make sure no two files share the same name
     */
    public void sameNameCheck() {
	
	//iterate through the whole table
	//don't include the last entry since we will end up comparing them
	for (int i = 0;i < tableSize - 1;i++) {
	    for (int k = i + 1;k < tableSize;k++) {
		
		//ensure we are comparing 2 files
		if ((table[i].inUse()) && (table[k].inUse())) {
		    
		    //if the two entries share a same name, report it
		    if (table[i].getName().equals(table[k].getName())) {
			Debug.println('+', "\t\tfile name " + table[i].getName() + " has multiple files associated with it");
		    }
		}
	    }
	}
    }
    
    /**
     * Check the directory to make sure that no files share the same file header
     */
    public void sameFileHeaderCheck() {
	
	//iterate through the whole table
	//don't include the last entry since we will end up comparing them
	for (int i = 0;i < tableSize - 1;i++) {
	    for (int k = i + 1;k < tableSize;k++) {
		
		//ensure we are comparing 2 files
		if ((table[i].inUse()) && (table[k].inUse())) {
		    
		    //if the two entries share a same sector, report it
		    if (table[i].getSector() == table[k].getSector()) {
			Debug.println('+', "\t\tfile " + table[i].getName() + " and file " + table[k].getName() + " refer to the same file header (sector " + table[i].getSector() + ")");
		    }
		}
	    }
	}
    }
    
    /**
     * Check all files to see if either a file uses a sector more than once, or if 2 or more files use the
     * same sector
     */
    public void sectorFileConsistencyCheck() {
	
	//create a list and a header
	ArrayList<Integer> list = new ArrayList<Integer>();
	FileHeader header = new FileHeader((FileSystemReal) Nachos.fileSystem);
	
	//iterate through the whole directory looking for open files
	for (int i = 0;i < tableSize;i++) {
	    
	    //if the entry is in use, open the file
	    if (table[i].inUse()) {
		
		//open the file and get the sectors it uses
		int sector = table[i].getSector();
		header.fetchFrom(sector);
		int[] fileSectors = checkMultipleSectors(header, table[i].getName());
		
		//add the sectors to the list
		for (int k = 0;k < fileSectors.length;k++) {
		    list.add(new Integer(fileSectors[k]));
		}
	    }
	}
	
	//once we have all of the unique sectors being used by files...
	//if the list contains copies, that means those sectors are being used by multiple files
	//check for copies and report them
	for (int i = 0;i < list.size() - 1;i++) {
	    for (int k = i + 1;k < list.size();k++) {
		if (list.get(i).intValue() == list.get(k).intValue()) {
		    Debug.println('+', "\t\tSector " + list.get(i).intValue() + " is being used across multiple files");
		}
	    }
	}
    }
    
    /**
     * Checks a file if it repeats sectors, and return an array of all of the unique sectors used
     */
    private int[] checkMultipleSectors(FileHeader header, String name) {
	
	//get the sectors used by an individual file
	int[] sectors = header.getSectors();
	
	
	//compare all sectors with each other to check for inconsistencies
	for (int i = 0;i < sectors.length - 1;i++) {
	    
	    //if we find a -1, don't check this number
	    if (sectors[i] == -1) {
		continue;
	    }
		
	    for (int k = i + 1;k < sectors.length;k++) {
		
		//if the file uses the same sector twice, then report it and
		if (sectors[i] == sectors[k]) {
		    Debug.println('+', "File " + name + " uses sector " + sectors[i] + " more than once");
		    sectors[k] = -1;
		}
	    }
	}
	
	//count up all of the sectors not marked with error
	int count = 0;
	for (int i = 0;i < sectors.length;i++) {
	    if (sectors[i] != -1) {
		count++;
	    }
	}
	
	//create a new array and copy the unique entries over
	int[] uniqueSectors = new int[count];
	count = 0;
	for (int i = 0;i < sectors.length;i++) {
	    if (sectors[i] != -1) {
		uniqueSectors[count] = sectors[i];
		count++;
	    }
	}

	//return the new array
	return uniqueSectors;
    }
    
    public boolean isInUse(int sector) { 
	for (int i = 0;i < table.length;i++) {
	    if (table[i].getSector() == sector) {
		return table[i].inUse();
	    }
	}
	return false;
    }
}
















