/* Basic test of Exec() system call */

#include "syscall.h"

int
main()
{
	Exec("test/exitTest");
	Exit(0);
}
