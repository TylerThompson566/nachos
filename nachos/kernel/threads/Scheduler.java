// Scheduler.java
//
//      readyToRun -- place a thread on the ready list and make it runnable.
//	finish -- called when a thread finishes, to clean up
//	yield -- relinquish control over the CPU to another ready thread
//	sleep -- relinquish control over the CPU, but thread is now blocked.
//		In other words, it will not run again, until explicitly 
//		put back on the ready queue.
// Copyright (c) 1992-1993 The Regents of the University of California.
// Copyright (c) 1998 Rice University.
// Copyright (c) 2003 State University of New York at Stony Brook.
// All rights reserved.  See the COPYRIGHT file for copyright notice and
// limitation of liability and disclaimer of warranty provisions.

package nachos.kernel.threads;


import java.util.ArrayList;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.kernel.userprog.UserThread;
import nachos.machine.CPU;
import nachos.machine.Machine;
import nachos.machine.NachosThread;
import nachos.machine.Simulation;
import nachos.machine.InterruptHandler;
import nachos.machine.Timer;
import nachos.util.*;

/**
 * The scheduler is responsible for maintaining a list of threads that
 * are ready to run and for choosing the next thread to run.  It also
 * maintains a list of CPUs that can be used to run threads.
 * Most of the public methods of this class implicitly operate on the
 * currently executing thread, with the exception of readyToRun,
 * which takes the thread to be placed in the ready list as an explicit
 * parameter.
 *
 * Mutual exclusion on the scheduler state uses two mechanisms:
 * (1) Disabling interrupts on the current CPU to prevent a timer
 * interrupt from re-entering the scheduler code.
 * (2) The current CPU obtains a spin lock in order to prevent concurrent
 * access to the scheduler state by other CPUs.
 * If there is just one CPU, then (1) would be enough.
 * 
 * The scheduling policy implemented here is very simple:
 * threads that are ready to run are maintained in a FIFO queue and are
 * dispatched in order onto the first available CPU.
 * Scheduling may be preemptive or non-preemptive, depending on whether
 * timers are initialized for time-slicing.
 * 
 * @author Thomas Anderson (UC Berkeley), original C++ version
 * @author Peter Druschel (Rice University), Java translation
 * @author Eugene W. Stark (Stony Brook University)
 */
public class Scheduler {

    /** Queue of threads that are ready to run, but not running. */
    private final ReadyList<NachosThread> readyList;

    /** Queue of CPUs that are idle. */
    private final Queue<CPU> cpuList;
    
    /** Terminated thread awaiting reclamation of its stack. */
    private volatile NachosThread threadToBeDestroyed;

    /** Spin lock for mutually exclusive access to scheduler state. */
    private final SpinLock mutex = new SpinLock("scheduler mutex");
    
    
    public ArrayList<UserThread> processTurnaroundTime;
    private boolean init = false;
    
    /** The method of scheduling that will be used by the scheduler 
      	0 = Default Nachos scheduling method
     	1 = First-come, first-served (FCFS, non-preemptive).
	2 = Round-robin (RR, preemptive).
	3 = Shortest process next (SPN, non-preemptive).
	4 = Shortest remaining time first (SRT, preemptive upon process arrival).
	5 = Highest response-ratio next (HRRN, non-preemptive).
	6 = Feedback scheduling (non-preemptive, 5 queues)*/
    private int scheduleMethod = 0;
    
    //constants for the schedule methods
    public final int FIRST_COME_FIRST_SERVED = 1;
    public final int ROUND_ROBIN = 2;
    public final int SHORTEST_PROCESS_NEXT = 3;
    public final int SHORTEST_REMAINING_TIME_FIRST = 4;
    public final int HIGHEST_RESPONSE_RATIO_NEXT = 5;
    public final int FEEDBACK_SCHEDULING = 6;

    
    /**
     * Initialize the scheduler with a scheduling method.
     * Set the list of ready but not running threads to empty.
     * Initialize the list of CPUs to contain all the available CPUs.
     * 
     * @param firstThread  The first NachosThread to run.
     */
    public Scheduler(NachosThread firstThread, int scheduleMethod) {
	
	//initialize schedule method and select which type of queue it will be
	this.scheduleMethod = scheduleMethod;
	switch (scheduleMethod) {
	    case FIRST_COME_FIRST_SERVED:
		readyList = new FCFSQueue<NachosThread>();
		break;
	    case ROUND_ROBIN:
		readyList = new RRQueue<NachosThread>();
		break;
	    case SHORTEST_PROCESS_NEXT:
		readyList = new SPNQueue<NachosThread>();
		break;
	    case SHORTEST_REMAINING_TIME_FIRST:
		readyList = new SRTFQueue<NachosThread>();
		break;
	    case HIGHEST_RESPONSE_RATIO_NEXT:
		readyList = new HRRNQueue<NachosThread>();
		break;
	    case FEEDBACK_SCHEDULING:
		readyList = new FSQueue<NachosThread>();
		break;
	    default:
		readyList = new FCFSQueue<NachosThread>();
	}
	cpuList = new FIFOQueue<CPU>();

	Debug.println('t', "Initializing scheduler with method: " + getSchedulingMethodString());

	// Add all the CPUs to the idle CPU list
	for(int i = 0; i < Machine.NUM_CPUS; i++) {
	    CPU cpu = Machine.getCPU(i);
	    cpuList.offer(cpu);
	    
	    if((scheduleMethod == ROUND_ROBIN) || (scheduleMethod == FEEDBACK_SCHEDULING)) {
		Timer timer = cpu.timer;
		timer.setHandler(new TimerInterruptHandler(timer.name));
		timer.start();
	    }
	}

	// Dispatch firstThread on the first CPU.
	CPU firstCPU = cpuList.poll();
	firstCPU.dispatch(firstThread);
    }
    
    public void initProcessTurnaroundTime(int processes) {
	processTurnaroundTime = new ArrayList<UserThread>();
	init = true;
    }

    /**
     * Stop the timers on all CPUs, in preparation for shutdown.
     */
    public void stop() {
	for(int i = 0; i < Machine.NUM_CPUS; i++) {
	    CPU cpu = Machine.getCPU(i);
	    cpu.timer.stop();
	}
    }

    /**
     * Mark a thread as ready, but not running, and put it on the ready list
     * for later scheduling onto a CPU.
     * If there are idle CPUs then threads are dispatched onto CPUs until either
     * all CPUs are in use or there are no more threads are ready to run.
     * 
     * It is assumed that multiple concurrent calls of this method will not be
     * made with the same thread as parameter, as that could result in the thread
     * being added more than once to the ready queue, in addition to introducing
     * the possibility of races in the setting of the thread status.
     *
     * @param thread The thread to be put on the ready list.
     */
    public void readyToRun(NachosThread thread) {
	int oldLevel = CPU.setLevel(CPU.IntOff);
	mutex.acquire();
	makeReady(thread);
	dispatchIdleCPUs();
	mutex.release();
	CPU.setLevel(oldLevel);
    }

    /**
     * Mark a thread as ready, but not running, and put it on the ready list
     * for later scheduling onto a CPU.
     * No attempt is made to dispatch threads on idle CPUs.
     * 
     * This internal version of readyToRun assumes that interrupts are disabled
     * and that the scheduler mutex is held.
     * It is assumed that multiple concurrent calls of this method will not be
     * made with the same thread as parameter.  Under that assumption, it is not
     * necessary to lock the thread object itself before changing its status to
     * READY, because any other changes to the thread status are either made by
     * the thread itself (which is currently not running), or in the process of
     * dispatching the thread, which is done with the scheduler mutex held.
     *
     * @param thread The thread to be put on the ready list.
     */
    private void makeReady(NachosThread thread) {
	Debug.ASSERT(CPU.getLevel() == CPU.IntOff && mutex.isLocked());

	Debug.println('t', "Putting thread on ready list: " + thread.name);

	thread.setStatus(NachosThread.READY);
	readyList.offer(thread);
	
	if (Nachos.options.STALLINGS_TEST && init) {
	    try {
		processTurnaroundTime.add((UserThread) thread);
	    } catch (Exception e) {}
	}
	
	// if we are doing SRTF and the new process is shorter than the current, switch processes
	if (scheduleMethod == SHORTEST_REMAINING_TIME_FIRST) {
	    try {
		UserThread newThread = (UserThread) thread;
		UserThread currentThread = (UserThread) NachosThread.currentThread();
		Debug.println('t', newThread.name + " burst length: " + newThread.burstTime);
		Debug.println('t', currentThread.name + " burst length: " + currentThread.burstTime);
		if (newThread.burstTime < currentThread.burstTime) {
		    Debug.println('t', "New Thread shorter than current, switching contexts");
		    yieldThread();
		}
	    } catch (Exception e) {
		Debug.println('t', "Exception thrown while checking burst times of threads");
	    }
	}
    }

    /**
     * If there are idle CPUs and threads ready to run, dispatch threads on CPUs
     * until either all CPUs are in use or no more threads are ready to run.
     * Assumes that interrupts have been disabled and that the scheduler mutex
     * is held.
     */
    private void dispatchIdleCPUs() {
	Debug.ASSERT(CPU.getLevel() == CPU.IntOff && mutex.isLocked());
	while(!readyList.isEmpty() && !cpuList.isEmpty()) {
	    NachosThread thread = readyList.poll();
	    CPU cpu = cpuList.poll();
	    Debug.println('t', "Dispatching " + thread.name + " on " + cpu.name);
	    try {
		if (((UserThread) thread).startingTick == 0) {
		    ((UserThread) thread).startingTick = Simulation.currentTime();
		}
	    } catch (Exception e) {}
	    cpu.dispatch(thread);
	    // The current CPU is not relinquished here -- immediate return.
	}
    }

    /**
     * Return the next thread to be scheduled onto a CPU.
     * If there are no ready threads, return null.
     * Side effect: thread is removed from the ready list.
     * Assumes that interrupts have been disabled.
     *
     * @return the thread to be scheduled onto a CPU.
     */
    private NachosThread findNextToRun() {
	Debug.ASSERT(CPU.getLevel() == CPU.IntOff);
	mutex.acquire();
	NachosThread result = readyList.poll();
	mutex.release();
	return result;
    }

    /**
     * Yield the current CPU, either to another thread, or else leave it idle.
     * Save the state of the current thread, and if a new thread is to be run,
     * dispatch it using the machine-dependent dispatcher routine: NachosThread.setCPU.
     * The thread that is yielding will either go back into the ready list for
     * rescheduling, or it will block, leaving the scheduler for an indefinite period
     * until something has again made it ready to run.
     * 
     * This method must be called with interrupts disabled.
     * When it eventually returns, the same will again be true.
     *
     * @param status  The status desired by the currently executing thread.
     * If RUNNING, then the thread will be put in the ready list and made READY,
     * unless there is no other thread to run, in which case it will be left RUNNING.
     * If BLOCKED the thread will be set to that status and will relinquish the CPU
     * to the next thread to run.
     * If FINISHED, it is assumed that the thread has already been set to that status,
     * and the thread will relinquish the CPU to the next thread to run.
     * @param  toRelease  If non-null, a spinlock held by the caller that is to be released
     * atomically with relinquishing the CPU.
     */
    private void yieldCPU(int status, SpinLock toRelease) {
	Debug.ASSERT(CPU.getLevel() == CPU.IntOff);
	CPU currentCPU = CPU.currentCPU();
	NachosThread currentThread = NachosThread.currentThread();
	NachosThread nextThread = findNextToRun();
	
	// If the current thread wants to keep running and there is no other thread to run,
	// do nothing.
	if(status == NachosThread.RUNNING && nextThread == null) {
	    Debug.println('t', "No other thread to run -- " + currentThread.name
		    			+ " continuing");
	    return;
	}
	
	Debug.println('t', "Next thread to run: "
		+ (nextThread == null ? "(none)" : nextThread.name));

	// The current thread will be suspending -- save its context.
	currentThread.saveState();

	mutex.acquire();
	if(toRelease != null)
	    toRelease.release();
	if(nextThread != null) {
	    // Switch the CPU from currentThread to nextThread.

	    Debug.println('t', "Switching " + CPU.getName() +
		    " from " + currentThread.name +
		    " to " + nextThread.name);

	    if(status == NachosThread.RUNNING) {
		// The current thread wants to keep running -- put it back in the ready list.
		makeReady(currentThread);
	    } else {
		// Set the new status of the thread before relinquishing the CPU.
		if(status != NachosThread.FINISHED)
		    currentThread.setStatus(status);
	    }
	    try {
		if (((UserThread) nextThread).startingTick == 0) {
		    ((UserThread) nextThread).startingTick = Simulation.currentTime();
		}
	    } catch (Exception e) {}
	    CPU.switchTo(nextThread, mutex);
	} else {
	    // There is nothing for this CPU to do -- send it to the idle list.

	    Debug.println('t', "Switching " + CPU.getName() +
		    " from " + currentThread.name +
		    " to idle");

	    cpuList.offer(currentCPU);
	    if(status != NachosThread.FINISHED)
		currentThread.setStatus(status);
	    currentCPU.timer.stop();
	    CPU.idle(mutex);
	}
	// Control returns here when currentThread has been rescheduled,
	// perhaps on a different CPU.
	Debug.ASSERT(CPU.getLevel() == CPU.IntOff);
	currentThread.restoreState();

	Debug.println('t', "Now in thread: " + currentThread.name);
    }

    /**
     * Relinquish the CPU if any other thread is ready to run.
     * If so, put the thread on the end of the ready list, so that
     * it will eventually be re-scheduled.
     *
     * NOTE: returns immediately if no other thread on the ready queue.
     * Otherwise returns when the thread eventually works its way
     * to the front of the ready list and gets re-scheduled.
     *
     * NOTE: we disable interrupts and acquire the scheduler mutex,
     * so that looking at the thread on the front of the ready list,
     * and switching to it, can be done atomically.  On return, we release
     * the scheduler mutex and re-set the interrupt level to its
     * original state.  This means this method will work properly no
     * matter whether interrupts are enabled or disabled when it is called,
     * but it should never be called with the scheduler mutex already locked.
     *
     * Similar to sleep(), but a little different.
     */
    public void yieldThread () {
	int oldLevel = CPU.setLevel(CPU.IntOff);

	Debug.println('t', "Yielding thread: " + NachosThread.currentThread().name);

	yieldCPU(NachosThread.RUNNING, null);
	// Control returns here when currentThread is rescheduled.

	CPU.setLevel(oldLevel);
    }

    /**
     * Relinquish the CPU, because the current thread is going to block
     * (i.e. either wait on a synchronization variable or, if the thread is finishing,
     * await final destruction).  If the thread is not finishing, then arrangements
     * should have been made for this thread to eventually be placed back on the ready
     * list, so that it can be re-scheduled.  If the thread is finishing, then
     * it will remain blocked until it is destroyed.
     *
     * NOTE: this method assumes interrupts are disabled, so that there can't be a time
     * slice between pulling the first thread off the ready list, and switching to it.
     * 
     * @param  toRelease  A spinlock held by the caller that is to be released atomically
     * with relinquishing the CPU.
     */
    public void sleepThread (SpinLock toRelease) {
	NachosThread currentThread = NachosThread.currentThread();
	Debug.ASSERT(CPU.getLevel() == CPU.IntOff);

	Debug.println('t', "Sleeping thread: " + currentThread.name);

	yieldCPU(NachosThread.BLOCKED, toRelease);
	// Control returns here when currentThread is rescheduled.
	// The caller is responsible for re-enabling interrupts.
    }

    /**
     * Relinquish the CPU for a specified number of ticks.
     * Uses the callout facility to arrange to wake up.
     * It is not necessary for interrupts to be disabled when calling this.
     */
    public void sleepThread(int ticks) {
	NachosThread t = NachosThread.currentThread();
	final Semaphore sem = new Semaphore("sleepThread: " + t.name, 0);
	Nachos.callout.schedule
		(new Runnable() {
		    public void run() {
			sem.V();
		    }
		}, ticks);
	// Block until awakened by callout.
	sem.P();
    }

    /**
     * Called by a thread to terminate itself.
     * A thread can't completely destroy itself, because it needs some
     * resources (e.g. a stack) as long as it is running.  So it is the
     * responsibility of the next thread to run to finish the job.
     */
    public void finishThread() {
	CPU.setLevel(CPU.IntOff);
	NachosThread currentThread = NachosThread.currentThread();
	
	try {
	   ((UserThread) currentThread).runningTime = Simulation.currentTime() - ((UserThread) currentThread).startingTick;
	} catch (Exception e){}

	Debug.println('t', "Finishing thread: " + currentThread.name);

	// We have to make sure the thread has been set to the FINISHED state
	// before making it the thread to be destroyed, because we don't want
	// someone to try to destroy a thread that is not FINISHED.
	currentThread.setStatus(NachosThread.FINISHED);
	
	// Delete the carcass of any thread that died previously.
	// This ensures that there is at most one dead thread ever waiting
	// to be cleaned up.
	mutex.acquire();
	if (threadToBeDestroyed != null) {
	    threadToBeDestroyed.destroy();
	    threadToBeDestroyed = null;
	}
	threadToBeDestroyed = currentThread;
	mutex.release();

	yieldCPU(NachosThread.FINISHED, null);
	// not reached

	// Interrupts will be re-enabled when the next thread runs or the
	// current CPU goes idle.
    }
    
    /** get the method for scheduling */
    public int getSchedulingMethod() {
	return scheduleMethod;
    }
    
    /** get a string version of the scheduling method */
    public String getSchedulingMethodString() {
	switch (scheduleMethod) {
		case (1):
		    return "First come, first serve";
		case (2):
		    return "Round-robin";
		case (3):
		    return "Shortest process next";
		case (4):
		    return "Shortest remaining time next";
		case (5):
		    return "Highest response-ratio next";
		case (6):
		    return "Feedback Scheduling";
	}
	return "Default NACHOS Scheduling";
    }
    
    /**
     * Interrupt handler for round robin. A timer is set up to
     * interrupt the CPU periodically (once every Timer.DefaultInterval ticks).
     * The handleInterrupt() method is called with interrupts disabled each
     * time there is a timer interrupt.
     */
    private static class TimerInterruptHandler implements InterruptHandler {

    	/** The Timer device this is a handler for. */
    	private final String timer;
    	
    	/** the tick of the last quantum */
        private int tick;
        
        /** the quantum of the scheduler */
        private final int QUANTUM = 1000;

    	/**
    	 * Initialize an interrupt handler for a specified Timer device.
    	 * 
    	 * @param timer  The device this handler is going to handle.
    	 */
    	public TimerInterruptHandler(String timer) {
    	    this.timer = timer;
    	    tick = Simulation.currentTime();
    	}

    	public void handleInterrupt() {
    	    Debug.println('i', "Scheduling Method: " + Nachos.scheduler.getSchedulingMethodString());
    	    Debug.println('i', "checking quantum expiration for timer " + timer + ":");
    	    Debug.println('i', "\tQuantum = " + QUANTUM);
    	    Debug.println('i', "\tLast burst tick = " + tick);
            
    	    //only yield on return if it is the quantum
    	    if (Simulation.currentTime() >= (tick + QUANTUM)) {
            
    		tick = Simulation.currentTime();
    		Debug.println('i', "quantum interrupt: " + timer);
    		// Note that instead of calling yield() directly (which would
    		// suspend the interrupt handler, not the interrupted thread
    		// which is what we wanted to context switch), we set a flag
    		// so that once the interrupt handler is done, it will appear as 
    		// if the interrupted thread called yield at the point it is 
    		// was interrupted.
    		yieldOnReturn();
    	    }
    	}

    	/**
    	 * Called to cause a context switch
    	 * in the interrupted thread when the handler returns.
    	 *
    	 * We can't do the context switch right here, because that would switch
    	 * out the interrupt handler, and we want to switch out the 
    	 * interrupted thread.  Instead, we set a hook to kernel code to be executed
    	 * when the current handler returns.
    	 */
    	private void yieldOnReturn() {
    	    Debug.println('i', "CPU Timer interrupt");
    	    CPU.setOnInterruptReturn
    	    (new Runnable() {
    		public void run() {
    		    if(NachosThread.currentThread() != null) {
    			Debug.println('t', "Yielding current thread on interrupt return");
    			Nachos.scheduler.yieldThread();
    		    } else {
    			Debug.println('i', "No current thread on interrupt return, skipping yield");
    		    }
    		}
    	    });
    	}

    }
}


/**
 * First Come First Served Queue'
 * @param queue The queue that holds all of the threads
 * @author Tyler
 *
 * @param <T>
 */
class FCFSQueue<T> implements ReadyList<T> {
    
    private FIFOQueue<NachosThread> queue;
    
    /** initialize the queue */
    public FCFSQueue() {
	queue = new FIFOQueue<NachosThread>();
    }

    /** add a thread to the queue */
    @Override
    public boolean offer(NachosThread e) {
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		if (((UserThread) e).firstTickActive != 0) {
		    ((UserThread) e).cpuTime += (Simulation.currentTime() - ((UserThread) e).firstTickActive);
		}
	    } catch (Exception ex){}
	}
	return queue.offer(e);
    }

    /** see what thread is going to come out of the queue next */
    @Override
    public NachosThread peek() {
	return queue.peek();
    }

    /** take the next thread out of the queue */
    @Override
    public NachosThread poll() {
	NachosThread thread = queue.poll();
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		((UserThread) thread).firstTickActive = Simulation.currentTime();
	    } catch (Exception ex){}
	}
	return thread;
    }

    /** check to see if the queue is empty or not */
    @Override
    public boolean isEmpty() {
	return queue.isEmpty();
    }
    
}

/**
 * Round Robin Queue
 * @param queue The queue that holds all of the threads
 * @author Tyler
 *
 * @param <T>
 */
class RRQueue<T> implements ReadyList<T> {
    
    private FIFOQueue<NachosThread> queue;
    
    /** initialize the queue */
    public RRQueue() {
	queue = new FIFOQueue<NachosThread>();
    }

    /** add a thread to the queue */
    @Override
    public boolean offer(NachosThread e) {
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		if (((UserThread) e).firstTickActive != 0) {
		    ((UserThread) e).cpuTime += (Simulation.currentTime() - ((UserThread) e).firstTickActive);
		}
	    } catch (Exception ex){}
	}
	return queue.offer(e);
    }

    /** see which thread is next to exit the queue */
    @Override
    public NachosThread peek() {
	return queue.peek();
    }

    /** remove the next thread from the queue */
    @Override
    public NachosThread poll() {
	NachosThread thread = queue.poll();
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		((UserThread) thread).firstTickActive = Simulation.currentTime();
	    } catch (Exception ex){}
	}
	return thread;
    }

    /** see if the queue is empty */
    @Override
    public boolean isEmpty() {
	return queue.isEmpty();
    }
}

/**
 * Shortest Process next queue
 * @param queue the queue that holds all of the threads
 * @author Tyler
 *
 * @param <T>
 */
class SPNQueue<T> implements ReadyList<T> {

    ArrayList<NachosThread> queue;
    
    /** initialize the queue */
    public SPNQueue() {
	queue = new ArrayList<NachosThread>();
    }
    
    /** add a thread to the queue */
    @Override
    public boolean offer(NachosThread e) {
	
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		if (((UserThread) e).firstTickActive != 0) {
		    ((UserThread) e).cpuTime += (Simulation.currentTime() - ((UserThread) e).firstTickActive);
		}
	    } catch (Exception ex){}
	}
	
	//update the remaining burst time and add it to the thread
	try {
	    UserThread addedThread = (UserThread) e;
		if (addedThread.runningTick != 0) {
		    addedThread.burstTime -= (Simulation.currentTime() - addedThread.runningTick);
		}
		if (addedThread.burstTime < 0) {
		    addedThread.burstTime = 0;
		}
	} catch (Exception ex) {}
	return queue.add(e);
    }

    /** see which thread is next to leave the queue */
    @Override
    public NachosThread peek() {
	
	//if the queue is not empty, continue
	if (!(queue.isEmpty())) {
	    
	    //if there is only one object in the queue, then return that object
	    UserThread thread = (UserThread) (queue.get(0));
	    
	    //iterate through the list
	    for (int i = 1;i < queue.size();i++) {
		
		//if the thread in the list has a shorter burst length, then set that one as the shortest
		UserThread listThread = (UserThread) queue.get(i);
		if (listThread.burstTime < thread.burstTime) {
		    thread = listThread;
		}
	    }
	    
	    //return the thread with the shortest burst length
	    return thread;
	}
	
	//if the queue is empty, return null
	return null;
    }

    /** remove the next thread to exit the queue */
    @Override
    public NachosThread poll() {
	
	//if the queue is not empty, continue
	if (!(queue.isEmpty())) {
	    
	    //start at index 0, and compare it first with the first thread. if there is one thread in the queue, then remove it and return it
	    int index = 0;
	    
	    //iterate through the list
	    for (int i = 1;i < queue.size();i++) {
		
		//if the list's thread is shorter than the current shortest one, then set the shortest one to the list's thread
		UserThread shortestThread = (UserThread) queue.get(index);
		UserThread queueThread = (UserThread) queue.get(i);
		if (shortestThread.burstTime > queueThread.burstTime) {
		    index = i;
		}
	    }
	    
	    //remove and return the thread with the shortest burst length
	    NachosThread returnThread = queue.get(index);
	    queue.remove(index);
	    if (Nachos.options.STALLINGS_TEST) {
		try {
		    ((UserThread) returnThread).firstTickActive = Simulation.currentTime();
		} catch (Exception ex){}
	    }
	    try {
		UserThread thread = (UserThread) queue.get(index);
		thread.runningTick = Simulation.currentTime();
	    }catch (Exception e){}
	    return returnThread;
	}
	
	//if the queue was empty, then return null
	return null;
    }

    @Override
    public boolean isEmpty() {
	return queue.isEmpty();
    }
}

class SRTFQueue<T> implements ReadyList<T> {

ArrayList<NachosThread> queue;
    
    /** initialize the queue */
    public SRTFQueue() {
	queue = new ArrayList<NachosThread>();
    }
    
    /** add a thread to the queue */
    @Override
    public boolean offer(NachosThread e) {
	
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		if (((UserThread) e).firstTickActive != 0) {
		    ((UserThread) e).cpuTime += (Simulation.currentTime() - ((UserThread) e).firstTickActive);
		}
	    } catch (Exception ex){}
	}
	
	//update the remaining burst time and add it to the thread
	try {
	    UserThread addedThread = (UserThread) e;
		if (addedThread.runningTick != 0) {
		    addedThread.burstTime -= (Simulation.currentTime() - addedThread.runningTick);
		}
		if (addedThread.burstTime < 0) {
		    addedThread.burstTime = 0;
		}
	} catch (Exception ex){}
	return queue.add(e);
    }

    /** see which thread is next to leave the queue */
    @Override
    public NachosThread peek() {
	
	//if the queue is not empty, continue
	if (!(queue.isEmpty())) {
		    
	    //start at index 0, and compare it first with the first thread. if there is one thread in the queue, then remove it and return it
	    int index = 0;
		    
	    //iterate through the list
	    for (int i = 1;i < queue.size();i++) {
			
		//if the list's thread is shorter than the current shortest one, then set the shortest one to the list's thread
		UserThread shortestThread = (UserThread) queue.get(index);
		UserThread queueThread = (UserThread) queue.get(i);
		if (shortestThread.burstTime > queueThread.burstTime) {
		    index = i;
		}
	    }
		    
	    //remove and return the thread with the shortest burst length
	    UserThread thread = (UserThread) queue.get(index);
	    return thread;
	}
		
	//if the queue was empty, then return null
	return null;
    }

    /** remove the next thread to exit the queue */
    @Override
    public NachosThread poll() {
	
	//if the queue is not empty, continue
	if (!(queue.isEmpty())) {
	    
	    //start at index 0, and compare it first with the first thread. if there is one thread in the queue, then remove it and return it
	    int index = 0;
	    
	    //iterate through the list
	    for (int i = 1;i < queue.size();i++) {
		
		//if the list's thread is shorter than the current shortest one, then set the shortest one to the list's thread
		UserThread shortestThread = (UserThread) queue.get(index);
		UserThread queueThread = (UserThread) queue.get(i);
		if (shortestThread.burstTime > queueThread.burstTime) {
		    index = i;
		}
	    }
	    
	    //remove and return the thread with the shortest burst length
	    NachosThread returnThread = queue.get(index);
	    if (Nachos.options.STALLINGS_TEST) {
		try {
		    ((UserThread) returnThread).firstTickActive = Simulation.currentTime();
		} catch (Exception ex){}
	    }
	    try {
		UserThread thread = (UserThread) queue.get(index);
		thread.runningTick = Simulation.currentTime();
	    } catch (Exception e){}
	    queue.remove(index);
	    return returnThread;
	}
	
	//if the queue was empty, then return null
	return null;
    }

    @Override
    public boolean isEmpty() {
	return queue.isEmpty();
    }
}

/**
 * Highest response ratio next queue
 * @author Tyler
 * @param queue the queue that holds all of the threads
 * @param <T>
 */
class HRRNQueue<T> implements ReadyList<T> {

    ArrayList<NachosThread> queue;
    
    public HRRNQueue() {
	queue = new ArrayList<NachosThread>();
    }

    /** add the thread to the list after setting its waiting tick */
    @Override
    public boolean offer(NachosThread e) {
	
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		if (((UserThread) e).firstTickActive != 0) {
		    ((UserThread) e).cpuTime += (Simulation.currentTime() - ((UserThread) e).firstTickActive);
		}
	    } catch (Exception ex){}
	}
	
	//update the remaining burst time and add it to the thread
	try {
	    UserThread addedThread = (UserThread) e;
		if (addedThread.runningTick != 0) {
		    addedThread.burstTime -= (Simulation.currentTime() - addedThread.runningTick);
		}
		if (addedThread.burstTime < 0) {
		    addedThread.burstTime = 0;
		}
		
		addedThread.waitingTick = Simulation.currentTime();
	} catch (Exception ex) {}
	return queue.add(e);
    }

    /** return the next thread to leave the queue */
    @Override
    public NachosThread peek() {
	
	//if the queue is not empty, then find which thread is next to leave
	if (!queue.isEmpty()) {

	    //start with the first thread and compare all threads with the current highest thread's response ratio
	    int highestRatioIndex = 0;
	    for (int i = 1;i < queue.size();i++) {
		if (getResponseRatio(queue.get(highestRatioIndex)) < getResponseRatio(queue.get(i))) {
		    highestRatioIndex = i;
		}
	    }
		    
	    //return the thread with the highest response ratio
	    NachosThread thread = queue.get(highestRatioIndex);
	    return thread;
	}
			
	//if the thread is empty, then return null
	return null;
    }

    /** give the calling thread the thread that is next to come from the queue */
    @Override
    public NachosThread poll() {
	
	//if the queue is not empty, then find which thread is next to leave
	if (!queue.isEmpty()) {
		    
	    //start with the first thread and compare all threads with the current highest thread's response ratio
	    int highestRatioIndex = 0;
	    for (int i = 1;i < queue.size();i++) {
		if (getResponseRatio(queue.get(highestRatioIndex)) < getResponseRatio(queue.get(i))) {
		    highestRatioIndex = i;
		}
	    }
	    
	    //get the thread with the highest response ratio
	    NachosThread thread = queue.get(highestRatioIndex);
	    
	    if (Nachos.options.STALLINGS_TEST) {
		try {
		    ((UserThread) thread).firstTickActive = Simulation.currentTime();
		} catch (Exception ex){}
	    }
	    
	    //update the waiting values for the thread
	    try {
		int timeInQueue = Simulation.currentTime() - ((UserThread) thread).waitingTick;
		    ((UserThread) thread).waitingTime += timeInQueue;
		    
		    //return the thread after updating its running tick and removing it from the list
		    ((UserThread) thread).runningTick = Simulation.currentTime();
	    } catch (Exception e){}
	    queue.remove(highestRatioIndex);
	    return thread;
	}
		
	//if the thread is empty, then return null
	return null;
    }

    /** determines if the list is empty or not */
    @Override
    public boolean isEmpty() {
	return queue.isEmpty();
    }
    
    /** get the response ratio given a certain thread */
    public double getResponseRatio(NachosThread e) {
	
	//w = (current total spent waiting time before it was added to queue) + (time spent waiting in queue)
	int timeInQueue = Simulation.currentTime() - ((UserThread) e).waitingTick;
	int w = ((UserThread) e).waitingTime + timeInQueue;
	int s = ((UserThread) e).burstTime;
	if (s == 0) {
	    s = 1;
	}
	return ((double) ((w + s) / s));
    }
}

/**
 * Feedback queue
 * @author Tyler
 * @param queue1 queue with priority 1 (highest)
 * @param queue2 queue with priority 2
 * @param queue3 queue with priority 3
 * @param queue4 queue with priority 4
 * @param queue5 queue with priority 5 (lowest, RR)
 * @param <T>
 */
class FSQueue<T> implements ReadyList<T> {

    FIFOQueue<NachosThread> queue1;	//Highest priority
    FIFOQueue<NachosThread> queue2;
    FIFOQueue<NachosThread> queue3;
    FIFOQueue<NachosThread> queue4;
    FIFOQueue<NachosThread> queue5;	//Lowest Priority
    
    /** initialize the queue */
    public FSQueue() {
	queue1 = new FIFOQueue<NachosThread>();
	queue2 = new FIFOQueue<NachosThread>();
	queue3 = new FIFOQueue<NachosThread>();
	queue4 = new FIFOQueue<NachosThread>();
	queue5 = new FIFOQueue<NachosThread>();
    }

    /** add the thread to the highest priority queue after shifting 1 thread from each queue down */
    @Override
    public boolean offer(NachosThread e) {
	
	if (Nachos.options.STALLINGS_TEST) {
	    try {
		if (((UserThread) e).firstTickActive != 0) {
		    ((UserThread) e).cpuTime += (Simulation.currentTime() - ((UserThread) e).firstTickActive);
		}
	    } catch (Exception ex){}
	}
	
	//if we can remove an object from a queue, then add it to the next lowest queue and continue.
	NachosThread thread1 = queue1.poll();
	if (thread1 != null) {
	    NachosThread thread2 = queue2.poll();
	    queue2.offer(thread1);
	    if (thread2 != null) {
		NachosThread thread3 = queue3.poll();
		queue3.offer(thread2);
		if (thread3 != null) {
		    NachosThread thread4 = queue4.poll();
		    queue4.offer(thread3);
		    if (thread4 != null) {
			NachosThread thread5 = queue5.poll();
			queue5.offer(thread4);
			if (thread5 != null) {
			    queue5.offer(thread5);
			}
		    }
		}
	    }
	}
	
	//add the new thread to the first queue
	return queue1.offer(e);
    }

    /** see which thread is the next to leave the queue */
    @Override
    public NachosThread peek() {
	if (!queue1.isEmpty()) {
	    return queue1.peek();
	}
	else if (!queue2.isEmpty()) {
	    return queue2.peek();
	}
	else if (!queue3.isEmpty()) {
	    return queue3.peek();
	}
	else if (!queue4.isEmpty()) {
	    return queue4.peek();
	}
	return queue5.peek();
    }

    /** remove a thread from the queue */
    @Override
    public NachosThread poll() {
	NachosThread returnThread;
	if (!queue1.isEmpty()) {
	    returnThread = queue1.poll();
	}
	else if (!queue2.isEmpty()) {
	    returnThread = queue2.poll();
	}
	else if (!queue3.isEmpty()) {
	    returnThread = queue3.poll();
	}
	else if (!queue4.isEmpty()) {
	    returnThread = queue4.poll();
	}
	else {
	    returnThread = queue5.poll();
	}
	if (Nachos.options.STALLINGS_TEST) {
		try {
		    ((UserThread) returnThread).firstTickActive = Simulation.currentTime();
		} catch (Exception ex){}
	    }
	return returnThread;
    }

    /** check to see if the queue is empty */
    @Override
    public boolean isEmpty() {
	if (!queue1.isEmpty()) {
	    return false;
	}
	else if (!queue2.isEmpty()) {
	    return false;
	}
	else if (!queue3.isEmpty()) {
	    return false;
	}
	else if (!queue4.isEmpty()) {
	    return false;
	}
	return queue5.isEmpty();
    }
}