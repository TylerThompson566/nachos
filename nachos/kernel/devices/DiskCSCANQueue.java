package nachos.kernel.devices;

import java.util.ArrayList;

import nachos.kernel.Nachos;
import nachos.util.DiskQueue;

/**
 * A disk scheduling queue that follows the CSCAN scheduling method
 * @author Tyler
 *
 * @param <T>
 */
public class DiskCSCANQueue<T> implements DiskQueue<T> {
    
    /** the queue that holds disk requests */
    ArrayList<DiskRequest> queue;
    
    public DiskCSCANQueue() {
	queue = new ArrayList<DiskRequest>();
    }

    @Override
    public boolean offer(DiskRequest e) {
	return queue.add(e);
    }

    /** get the next request to be handled without removing it from the list */
    @Override
    public DiskRequest peek() {
	
	//return null if the queue is empty
	if (queue.isEmpty()) {
	    return null;
	}
	
	//if the queue size is 1, just return that element
	if (queue.size() == 1) {
	    return queue.get(0);
	}
	
	//create an initial request to return, and get the sector number of the request being worked
	DiskRequest request = queue.get(0);
	int currentRequestSectNum = Nachos.diskDriver.getCurrentRequest().sectorNumber;
	
	//find a request that is at the closest sector after the current request
	for (int i = currentRequestSectNum;i < Nachos.diskDriver.getNumSectors();i++) {
	    for (int k = 0;k < queue.size();k++) {
		if (queue.get(k).sectorNumber == i) {
		    return queue.get(k);
		}
	    }
	}
	
	//if we reached here, there are no requests at any sectors after the current request
	//find the request that is closest to the beginning
	for (int i = 0;i < Nachos.diskDriver.getNumSectors();i++) {
	    for (int k = 0;k < queue.size();k++) {
		if (queue.get(k).sectorNumber == i) {
		    return queue.get(k);
		}
	    }
	}
	
	//return whatever request request equals
	return request;
    }

    /** remove and return the next request to be fulfilled */
    @Override
    public DiskRequest poll() {
	
	//return null if the queue is empty
	if (queue.isEmpty()) {
	    return null;
	}
	
	//if the queue size is 1, just return that element
	if (queue.size() == 1) {
	    DiskRequest onlyRequest = queue.get(0);
	    queue.remove(0);
	    return onlyRequest;
	}
	
	//create an initial request to return, and get the sector number of the request being worked
	DiskRequest request = queue.get(0);
	int currentRequestSectNum = Nachos.diskDriver.getCurrentRequest().sectorNumber;
	
	//find a request that is at the closest sector after the current request
	for (int i = currentRequestSectNum;i < Nachos.diskDriver.getNumSectors();i++) {
	    for (int k = 0;k < queue.size();k++) {
		if (queue.get(k).sectorNumber == i) {
		    request = queue.get(k);
		    queue.remove(k);
		    return request;
		}
	    }
	}
	
	//if we reached here, there are no requests at any sectors after the current request
	//find the request that is closest to the beginning
	for (int i = 0;i < Nachos.diskDriver.getNumSectors();i++) {
	    for (int k = 0;k < queue.size();k++) {
		if (queue.get(k).sectorNumber == i) {
		    request = queue.get(k);
		    queue.remove(k);
		    return request;
		}
	    }
	}
	
	//return whatever request request equals
	queue.remove(request);
	return request;
    }

    @Override
    public boolean isEmpty() {
	return queue.isEmpty();
    }
    
}