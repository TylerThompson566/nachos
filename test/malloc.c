/* Implementation of Malloc and free */

#include "syscall.h"

/* the memory region node for the free list */
struct memory_region {
	struct memory_region *next;	/* the next node in the list */
	int size;					/* how large the memory region is */
	int data[0];				/* the data starts here and continues on */
};

/* global variables */
static struct memory_region *firstfree;	/* the head of the free list */
static int calledMalloc = 0;			/* if we called malloc already */


/* malloc function */
void *malloc(unsigned int size) {

	/* create a cursor for searching the free list */
	struct memory_region *return_node = NULL;
	
	/* start looking for blocks at the first free block */
	struct memory_region *cursor = firstfree;


	/* if we have not called malloc yet, we need to init the free list */
	if (calledMalloc == 0) {
		heap_limit = heap_limit + sizeof(int);
		firstfree = (struct memory_region *)heap_start;
    	firstfree->next = NULL;
    	firstfree->size = heap_limit - heap_start;
        
    	/* we called malloc so we don't need to init the list anymore */
    	calledMalloc = 1;
	}
	
	/* check the first block if it is good */
	if ((firstfree != NULL) && (firstfree->size >= size)) {
		return_node = firstfree;
		firstfree = firstfree->next;
	}
	
	/* otherwise, search the free list for a free block that has a big enough size */
	else {
		while (cursor->next != NULL) {
	
			/* if we find a node that is big enough, then return the pointer to the space */
			if (((cursor->next)->size) >= size) {
			
				/* save the cursor that we are returning */
				return_node = cursor->next;
			
				/* remove the node we are returning from the list */
				cursor->next = (cursor->next)->next;
			}
		
			/* otherwise, get the next block in the list */
			cursor = cursor->next;
		}
	}
	
	/* if we do not find anything, then increase the heap limit */
	if (return_node == NULL) {
		void *old_heap_limit = heap_limit;
		heap_limit += (size + sizeof(int) + sizeof(struct memory_region *));
		return_node = (struct memory_region *) old_heap_limit;
		return_node->size = size;
	}
	
	/* return the space of the node we find */
	return (return_node + sizeof(int) + sizeof(struct memory_region *));
}

/* free function */
void free(void *ptr) {

	/* get the block pointer from the space pointer returned by malloc */
	struct memory_region *pp = (struct memory_region *) (ptr - sizeof(int) - sizeof(struct memory_region *));
	
	/* create a cursor in case we need it */
	struct memory_region *cursor = firstfree;
    
    /* this block is going at the end, so set its next to NULL */
    pp->next = NULL;
    
    /* if the free list is empty, then set the head to the added block */
    if (firstfree == NULL) {
    	firstfree = pp;
    }
    
    /* otherwise, traverse the list */
    else {
    	while (cursor != NULL) {
    
    		/* if the cursor's next is null, set the next to the block and break */
    		if (cursor->next == NULL) {
    			cursor->next = pp;
    			break;
    		}
    	}
    }
}