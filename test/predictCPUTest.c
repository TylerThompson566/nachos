/* Basic test of PredictCPU() system call */
/* n = number of times the loop will iterate */
/* 13 is the number of instructions 1 loop will take when doing 'k = 0' */
/* since we know how many instructions there are in one loop and how many times we iterate, it will take */
/* 10x13 instructions to  */

#include "syscall.h"

int main() {
	int n = 100;
	int i;
	int k = 0;
    PredictCPU(n * 13);
    Print(n);
    for(i = 0; i < n; i++) {
    	k = 0;
    }
}