package nachos.util;

import nachos.kernel.devices.DiskRequest;

/**
 * A list of disk read/write requests that can be handled.
 * @author Tyler
 *
 * @param <T> The list of read/write requests
 */
public interface DiskQueue<T> {
    
    /**
     * Adds an element to this queue, if it is possible to do so immediately
     * without violating capacity restrictions.
     * 
     * @param e  The element to add.
     * @return  true if the element was successfully added, false if the element
     * was not added.
     */
    public boolean offer(DiskRequest e);
    
    /**
     * Retrieves, but does not remove, the head of this queue, or returns null
     * if this queue is empty.
     * 
     * @return  The element at the head of the queue, or null if the queue is
     * empty.
     */
    public DiskRequest peek();
    
    /**
     * Retrieves and removes the head of this queue, or returns null if this queue is empty.
     * 
     * @return  the head of this queue, or null if this queue is empty.
     */
    public DiskRequest poll();
    
    /**
     * Test whether this queue is currently empty.
     * 
     * @return true if this queue is currently empty.
     */
    public boolean isEmpty();
}