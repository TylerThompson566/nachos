package nachos.kernel.threads.test;

import java.util.Random;

import nachos.Debug;
import nachos.kernel.Nachos;
import nachos.util.SynchronousQueue;
import nachos.machine.NachosThread;
import nachos.machine.Simulation;

/**
 * Simple test of the synchronous queue class.
 * 
 * @author Eugene W. Stark
 * @version 20140304
 */
public class SynchronousQueueTest {

    private static final int NUM_PUTS = 20;
    
    // Use the RNG from Simulation in order to get repeatability.
    private static final Random random = Simulation.random;
    
    private static final SynchronousQueue<String> queue = new SynchronousQueue<String>();

    public static void start() {
	Debug.println('+', "Entering SynchronousQueueTest");
	for(int i = 0; i < NUM_PUTS; i++) {
	    final String name = "SynchronousQueueTest " + i;
	    
	    final int putDelay = random.nextInt(10000);
	    final boolean putTimeout = random.nextBoolean();
	    final int putTicks = random.nextInt(1000);
	    final NachosThread putter =
		    new NachosThread(name + (putTimeout ? (" (offer: " + putTicks + ")") : " (put)"));
	    putter.setRunnable
	    (new Runnable() {
		public void run() {
		    Nachos.scheduler.sleepThread(putDelay);
		    Debug.println('+', "Thread " + putter.name + " putting");
		    boolean b = queue.put(name, putTimeout ? putTicks : -1);
		    Debug.println('+', "Thread " + putter.name + (!b ? " timed out" : " succeeded"));
		    Nachos.scheduler.finishThread();
		}
	    });
	    Nachos.scheduler.readyToRun(putter);
	    
	    final int takeDelay = random.nextInt(10000);
	    final boolean takeTimeout = random.nextBoolean();
	    final int takeTicks = random.nextInt(1000);
	    final NachosThread taker =
		    new NachosThread(name + (takeTimeout ? (" (poll: " + takeTicks + ")") : " (take)"));
	    taker.setRunnable
	    (new Runnable() {
		public void run() {
		    Nachos.scheduler.sleepThread(takeDelay);
		    Debug.println('+', "Thread " + taker.name + " taking");
		    String s = queue.take(takeTimeout ? takeTicks : -1);
		    Debug.println('+', "Thread " + taker.name
			    		+ (s == null ? " timed out" :  " returned  (" + s + ")"));
		    Nachos.scheduler.finishThread();
		}
	    });
	    Nachos.scheduler.readyToRun(taker);
	}
    }

}
