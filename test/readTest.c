/* readTest.c
 *	Simple program to test whether the read syscall works or not
 */

#include "syscall.h"

int
main()
{
    char buffer[10];
    int i = 0;
    for (i = 0;i < 10;i++) {
    	buffer[i] = 0;
    }
    Read(buffer, 10, 0);
    
    Exit(0);
}