/* Basic test of malloc() and free() functions */

#include "syscall.h"

int
main()
{
	int *ptr = (int *) malloc(sizeof(int));
	*ptr = 1;
	Print(*ptr);
	free(ptr);
	*ptr = (int *) malloc(sizeof(int));
	*ptr = 2;
	Print(*ptr);
	free(ptr);
	*ptr = (int *) malloc(sizeof(int));
	*ptr = 3;
	Print(*ptr);
	free(ptr);
	Exit(0);
}
