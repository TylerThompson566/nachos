// Copyright (c) 2003 State University of New York at Stony Brook.
// All rights reserved.  See the COPYRIGHT file for copyright notice and
// limitation of liability and disclaimer of warranty provisions.

package nachos.kernel.userprog;

import java.io.UnsupportedEncodingException;

import nachos.Debug;
import nachos.machine.CPU;
import nachos.machine.MIPS;
import nachos.machine.Machine;
import nachos.machine.MachineException;
import nachos.machine.NachosThread;
import nachos.kernel.userprog.Syscall;

/**
 * An ExceptionHandler object provides an entry point to the operating system
 * kernel, which can be called by the machine when an exception occurs during
 * execution in user mode.  Examples of such exceptions are system call
 * exceptions, in which the user program requests service from the OS,
 * and page fault exceptions, which occur when the user program attempts to
 * access a portion of its address space that currently has no valid
 * virtual-to-physical address mapping defined.  The operating system
 * must register an exception handler with the machine before attempting
 * to execute programs in user mode.
 */
public class ExceptionHandler implements nachos.machine.ExceptionHandler {

  /**
   * Entry point into the Nachos kernel.  Called when a user program
   * is executing, and either does a syscall, or generates an addressing
   * or arithmetic exception.
   *
   * 	For system calls, the following is the calling convention:
   *
   * 	system call code -- r2,
   *		arg1 -- r4,
   *		arg2 -- r5,
   *		arg3 -- r6,
   *		arg4 -- r7.
   *
   *	The result of the system call, if any, must be put back into r2. 
   *
   * And don't forget to increment the pc before returning. (Or else you'll
   * loop making the same system call forever!)
   *
   * @param which The kind of exception.  The list of possible exceptions 
   *	is in CPU.java.
   *
   * @author Thomas Anderson (UC Berkeley), original C++ version
   * @author Peter Druschel (Rice University), Java translation
   * @author Eugene W. Stark (Stony Brook University)
   */
    public void handleException(int which) {
	int type = CPU.readRegister(2);

	if (which == MachineException.SyscallException) {

	    switch (type) {
	    
	    	case Syscall.SC_Halt:
	    	    Syscall.halt();
	    	    break;
		
	    	case Syscall.SC_Exit:
	    	    Syscall.exit(CPU.readRegister(4));
	    	    break;
		
	    	case Syscall.SC_Exec:
		
	    	    //get the arguments of the syscall from the registers
	    	    int execptr = CPU.readRegister(4);	//the pointer to the string in main memory
	    	    int counter = execptr;		//the counter to keep track of where we are in the string while we loop
	    	    int execlen = 0;			//the length of the string
		
	    	    //loop until we find the null terminator
	    	    //increment counter to get the next character
	    	    //increment execlen to say we found 1 more character in the string
	    	    while (Machine.mainMemory[counter] != '\0') {
	    		counter++;
	    		execlen++;
	    	    }
		
	    	    //create a buffer that will store all of the bytes in the string
	    	    byte execbuf[] = new byte[execlen];

	    	    //copy the bytes into the array
	    	    System.arraycopy(Machine.mainMemory, execptr, execbuf, 0, execlen);
		
	    	    //create a string that will be passed to Syscall.exec()
	    	    String execstr = "";
		
	    	    //try to parse the byte array into a string, encoding it in UTF-8
	    	    try {
	    		execstr = new String(execbuf, "UTF-8");
	    	    } catch (UnsupportedEncodingException e) {
	    		e.printStackTrace();
	    	    }
		
	    	    //call the syscall and break
	    	    int spaceID = Syscall.exec(execstr);
	    	    
	    	    //write the result to the return register
	    	    CPU.writeRegister(2, spaceID);
	    	    break;
	    	
	    	case Syscall.SC_Join:
	    	    int exitStatus = Syscall.join(CPU.readRegister(4));
	    	    CPU.writeRegister(2, exitStatus);
	    	    break;
	    	    
	    	case Syscall.SC_Read:
	    	    int readptr = CPU.readRegister(4);
	    	    int readlen = CPU.readRegister(5);
	    	    byte readbuf[] = new byte[readlen];
	    	    
	    	    int bytesRead = Syscall.read(readbuf, readlen, CPU.readRegister(6));
	    	    System.arraycopy(readbuf, 0, Machine.mainMemory, readptr, readlen);
	    	    CPU.writeRegister(2, bytesRead);
	    	    break;
		
	    	case Syscall.SC_Write:
	    	    int writeptr = CPU.readRegister(4);
	    	    int writelen = CPU.readRegister(5);
	    	    byte writebuf[] = new byte[writelen];

	    	    System.arraycopy(Machine.mainMemory, writeptr, writebuf, 0, writelen);
	    	    Syscall.write(writebuf, writelen, CPU.readRegister(6));
	    	    break;
	    	    
	    	case Syscall.SC_Fork:
	    	    int func = CPU.readRegister(4);
	    	    Syscall.fork(func);
	    	    break;
	    	    
	    	case Syscall.SC_Yield:
	    	    Syscall.yield();
	    	    break;
	    	    
	    	case Syscall.SC_PredictCPU:
	    	    int ticks = CPU.readRegister(4);
	    	    Syscall.predictCPU(ticks);
	    	    break;
	    	    
	    	case Syscall.SC_Print:
	    	    int print = CPU.readRegister(4);
	    	    Syscall.print(print);
	    	    break;
	    	}

	    // Update the program counter to point to the next instruction
	    // after the SYSCALL instruction.
	    CPU.writeRegister(MIPS.PrevPCReg,
		CPU.readRegister(MIPS.PCReg));
	    CPU.writeRegister(MIPS.PCReg,
		CPU.readRegister(MIPS.NextPCReg));
	    CPU.writeRegister(MIPS.NextPCReg,
		CPU.readRegister(MIPS.NextPCReg)+4);
	    return;
	}
	
	//if a process attempts to access a virtual address higher than any existing valid address
	else if (which == MachineException.AddressErrorException) {
	    
	    //handle the address error exception
	    (((UserThread) NachosThread.currentThread()).space).handleAddressErrorException();
	    
	    return;
	}
	
	//if a process accesses a page in VM that is invalid
	else if (which == MachineException.PageFaultException) {
	    
	    //get the current address space and handle the page fault exception
	    AddrSpace space = ((UserThread) NachosThread.currentThread()).space;
	    space.handlePageFaultException();
	    
	    return;
	}
	
	else if (which == MachineException.ReadOnlyException) {
	    
	    
	    Debug.ASSERT(false, "Read only Exception");
	}

	System.out.println("Unexpected user mode exception " + which +
		", " + type);
	Debug.ASSERT(false);

    }
}
