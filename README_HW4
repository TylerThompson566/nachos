Tyler Thompson, 109073653

Homework 4 README

Summary

Starting with the disk driver, modifying DiskDriver.java to make it work
with multiple threads was straight forward. First, I removed the lock that
made it so only one thread could access the driver at one point in time. 
Instead, I implemented a DiskQueue that could be used as a request queue
for disk read/write requests. The DiskQueue interface can be used as either
a CSCAN disk queue, or a FCFS disk queue, depending on the arguments provided
at start up. The disk driver uses this queue to store requests so they can
be serviced at the appropriate time. I also added a DiskRequest object that
holds all of the information needed to schedule a read/write request to the disk.
The DiskDriver holds the current request being worked on, and the queue of
pending requests. When a request comes in from the top half of the driver,
the DiskRequest is made, and it is passed to a method handleRequest, which
adds the request to the queue and calls P on the semaphore that is a part
of the request object. Once a request is finished being served, the lower
half of the driver calls V on the semaphore held by the DiskRequest object
that is stopping the thread waiting for its disk request to be serviced. Once
V is called, the thread waiting on that request can continue running. 

CSCAN is implemented by iterating through all of the sectors starting from the 
sector where the last request was serviced from. It iterates through the disk
sectors until it finds a sector that needs to be serviced due to a disk request.
If it reaches the end, it starts over again from sector 0. if it does not find anyhting,
the queue is empty.

FCFS is implemented using just a FIFOQueue

To test the functionality of CSCAN, I created a DiskSchedulingTest class that can
be run like any other test. This creates a starting request, and then 
multiple other requests with random sectors. the thread adds these requests
to the queue, and simulates dequeueing based on the specified disk scheduling
method at runtime.

For the file system, I initialized FILESYS_STUB=false and FILESYS_REAL=true in 
Nachos.java. this used the real file system rather than the stub version.
I also set FILESYS_TEST to true, so i can use the disk.

For thread safety, I created a FileHeaderManager class. This manager maintains a
hashtable where the keys are integers and the values are semaphores. 
The integers represent a sector where a file header is contained. when a thread
wants to create a file header, It must call waitForAccess from the file header
manager. This searches the hash table for the sector specified, and if it is found,
it calls P on the semaphore associated with the key, and returns as soon as it can.
This means that only one thread at a time can access a file header at a specific
sector. Any other thread must wait until a thread holding the semaphore for that
file header must call returnAccess in order to call V on the semaphore it called P
on when it waited for access. When a file is deleted, we call removeEntry that removes
an entry in the hashtable atomically.

The calls have a specific order. 
WaitForAccess(DirectorySector)
WaitForAccess(FreeMapSector)
WaitForAccess(FileSector)
ReturnAccess(FileSector)
ReturnAccess(FreeMapSector)
ReturnAccess(DirectorySector)
This ensures that no thread can create a deadlock with another thread by waiting for
a thread to relinquish a lock on a thread that is waiting for the other thread to 
relinquish a lock that it is holding. 

The consistency check is a method in FileSystemReal called checkConsistency. This method
calls several helper methods that check specific errors in the file system.
SameNameCheck checks for files that have the same name
SameFileHeaderCheck checks the directory for separate directory entries referring
to the same sector
UsedButMarkedFreeCheck checks all used directory entries and checks to see if all 
of the used directory entries are marked used in the bit map
UnusedButMarkedInUseCheck checks all marked in use sectors in the bitmap and checks
to see if they all have an entry in the directory. 
sectorFileConsistencyCheck checks all file headers to ensure that two different file
headers refer to the same sector, or if a single file header uses the same sector
more than once. 

The stress test creates 2 threads that read, write, create, and delete files. It
first creates 4 files file0-file3. The different threads use a pseudo-random number
to determine which of the four actions to do. They will do this 10 times then the
last thread to finish will run the consistency check to ensure that everything works
properly.


TESTING

To change the disk schedule method, use the flag -dsm <scheduling method>
	0 = CSCAN
	1 = FCFS
To check the implementation of CSCAN, use the flag -dst
this will run the DiskSchedulingTest
	You need to specify which disk scheduling method you want to use to ensure
	you are testing the correct scheduling method
to do the stress test, use the flag -fsst
	the file system stress test creates 4 files to work with, so ensure that there
	is enough disk space to create these 4 files.