// ThreadTest.java
//	Simple test class for the threads assignment.
//
//	Create two threads, and have them context switch
//	back and forth between themselves by calling yield(), 
//	to illustrate the thread system.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// Copyright (c) 1998 Rice University.
// Copyright (c) 2003 State University of New York at Stony Brook.
// All rights reserved.  See the COPYRIGHT file for copyright notice and
// limitation of liability and disclaimer of warranty provisions.

package nachos.kernel.userprog.test;

import nachos.Debug;
import nachos.machine.NachosThread;
import nachos.kernel.Nachos;
import nachos.kernel.userprog.AddrSpace;

/**
 * test paging
 */
public class PagingTest implements Runnable {
    

    public PagingTest() {
	NachosThread t = new NachosThread("paging test thread", this);
	Nachos.scheduler.readyToRun(t);
    }
    
    
    public void run() {
	
	System.out.println("Creating testing address space\n");
	AddrSpace space = new AddrSpace();
	space.initPagingTest();
	
	Nachos.scheduler.finishThread();
    }
    
    /**
     * Entry point for the test.
     */
    public static void start() {
	Debug.println('+', "Entering ThreadTest");
	new PagingTest();
    }

}
