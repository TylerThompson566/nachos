// StallingsTest.java
//	Simple test class for the threads assignment.
//
//	Randomly sleeps for a certain amount of time then chooses a random number
//	to iterate in StallingsTest.c 

package nachos.kernel.threads.test;

import java.util.ArrayList;

import nachos.Debug;
import nachos.machine.CPU;
import nachos.machine.NachosThread;
import nachos.kernel.Nachos;
import nachos.kernel.filesys.OpenFile;
import nachos.kernel.userprog.AddrSpace;
import nachos.kernel.userprog.ProgRun;
import nachos.kernel.userprog.Syscall;
import nachos.kernel.userprog.UserThread;

/**
 * Set up a thread that will randomly sleep and then iterate through StallingsTest.c
 *     
 * @author Tyler
 */
public class StallingsTest implements Runnable {

    /** the number of processes that the test will iterate through */
    private int processes;
    
    /** the name of the StallingsTest.c process */
    private String name = "test/StallingsTest";

    /**
     * Initialize an instance of StallingsTest and start a new thread running
     * on it.
     *
     * @param processes the number of times the thread will launch StallingsTest.c
     */
    public StallingsTest(int processes) {
	this.processes = processes;
	
	NachosThread t = new NachosThread("Stallings Test Thread", this);
	Nachos.scheduler.readyToRun(t);
    }

    /**
     * 
     */
    public void run() {
	
	Nachos.scheduler.initProcessTurnaroundTime(processes);
	
	//iterate (processes) times
	for (int i = 0;i < processes;i++) {
	    
	    //launch StallingsTest.c
		
	    //create the space and an open file
	    AddrSpace space = new AddrSpace();
	    OpenFile executable;

	    //if we cannot open the file, then stop executing the thread
	    if((executable = Nachos.fileSystem.open(name)) == null) {
		Debug.println('+', "Unable to open executable file: " + name);
		Nachos.scheduler.finishThread();
	    }
	    
	    //if we cannot load the program into memory, then return an error code
	    if(space.exec(executable) == -1) {
		Debug.println('+', "Unable to read executable file: " + name);
		Nachos.scheduler.finishThread();
	    }
	    
	    //start the process
	    ProgRun.start(new String("StallingsTest" + (i + 1)), space, getRandomIterations());
	    
	    //add the process to the process list in the memory manager
	    Nachos.memoryManager.addProcess(space.getSpaceID());
	    
	    //determine how long to sleep for
	    int sleepTime = getRandomSleepTime();
	    Nachos.scheduler.sleepThread(sleepTime);
	}
	
	//print the contents of the test
	System.out.println("Stalling's Test results");
	for (int i = 0;i < Nachos.scheduler.processTurnaroundTime.size();i++) {
	    System.out.println("\tProcess " + i + ": " + Nachos.scheduler.processTurnaroundTime.get(i).runningTime + " ticks");
	}
	
	ArrayList<UserThread> percent1 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent10 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent20 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent30 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent40 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent50 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent60 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent70 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent80 = new ArrayList<UserThread>();
	ArrayList<UserThread> percent90 = new ArrayList<UserThread>();
	
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		    
		}
	    }
	    percent1.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent10.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent20.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent30.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent40.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent50.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent60.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent70.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent80.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	for (int i = 0;i < processes / 10;i++) {
	    UserThread lowest = Nachos.scheduler.processTurnaroundTime.get(0);
	    for (int k = 0;k < Nachos.scheduler.processTurnaroundTime.size();k++) {
		if (lowest.runningTime > Nachos.scheduler.processTurnaroundTime.get(k).runningTime) { 
		    lowest = Nachos.scheduler.processTurnaroundTime.get(k);
		}
	    }
	    percent90.add(lowest);
	    Nachos.scheduler.processTurnaroundTime.remove(lowest);
	}
	
	System.out.println("0% - 9%");
	for (int i = 0;i < percent1.size();i++) {
	    System.out.println("\t" + percent1.get(i).name + "\n\tturnaround: " + percent1.get(i).runningTime + "\n\tcpu: " + percent1.get(i).cpuTime);
	}
	System.out.println("10% - 19%");
	for (int i = 0;i < percent10.size();i++) {
	    System.out.println("\t" + percent10.get(i).name + "\n\tturnaround: " + percent10.get(i).runningTime + "\n\tcpu: " + percent10.get(i).cpuTime);
	}
	System.out.println("20% - 29%");
	for (int i = 0;i < percent20.size();i++) {
	    System.out.println("\t" + percent20.get(i).name + "\n\tturnaround: " + percent20.get(i).runningTime + "\n\tcpu: " + percent20.get(i).cpuTime);
	}
	System.out.println("30% - 39%");
	for (int i = 0;i < percent30.size();i++) {
	    System.out.println("\t" + percent30.get(i).name + "\n\tturnaround: " + percent30.get(i).runningTime + "\n\tcpu: " + percent30.get(i).cpuTime);
	}
	System.out.println("40% - 49%");
	for (int i = 0;i < percent40.size();i++) {
	    System.out.println("\t" + percent40.get(i).name + "\n\tturnaround: " + percent40.get(i).runningTime + "\n\tcpu: " + percent40.get(i).cpuTime);
	}
	System.out.println("50% - 59%");
	for (int i = 0;i < percent50.size();i++) {
	    System.out.println("\t" + percent50.get(i).name + "\n\tturnaround: " + percent50.get(i).runningTime + "\n\tcpu: " + percent50.get(i).cpuTime);
	}
	System.out.println("60% - 69%");
	for (int i = 0;i < percent60.size();i++) {
	    System.out.println("\t" + percent60.get(i).name + "\n\tturnaround: " + percent60.get(i).runningTime + "\n\tcpu: " + percent60.get(i).cpuTime);
	}
	System.out.println("70% - 79%");
	for (int i = 0;i < percent70.size();i++) {
	    System.out.println("\t" + percent70.get(i).name + "\n\tturnaround: " + percent70.get(i).runningTime + "\n\tcpu: " + percent70.get(i).cpuTime);
	}
	System.out.println("80% - 89%");
	for (int i = 0;i < percent80.size();i++) {
	    System.out.println("\t" + percent80.get(i).name + "\n\tturnaround: " + percent80.get(i).runningTime + "\n\tcpu: " + percent80.get(i).cpuTime);
	}
	System.out.println("90% - 99%");
	for (int i = 0;i < percent90.size();i++) {
	    System.out.println("\t" + percent90.get(i).name + "\n\tturnaround: " + percent90.get(i).runningTime + "\n\tcpu: " + percent90.get(i).cpuTime);
	}
	
	
	
	//end the thread
	Nachos.scheduler.finishThread();
    }
    
    /** get the random number of iterations a process will loop for */
    public int getRandomIterations() {
	
	//initialize needed variables
	double prob = Math.random();
	int iterations = 100;
	double lastProb = 0;
	for (int i = 0;i < 100;i++) {
	    if (prob < (lastProb + (Math.pow(0.9, i) * 0.1))) {
		double prob2 = Math.random();
		prob2 *= 100;
		prob2++;
		iterations = (int) prob2;
		iterations += (100 * i);
		break;
	    }
	    lastProb += (Math.pow(0.9, i) * 0.1);
	}
	System.out.println("iterations = " + iterations);
	return iterations;
    }
    
    /** get the random ticks to sleep for */
    public int getRandomSleepTime() {
	
	//initialize needed variables
	double prob = Math.random();
	int ticks = 1000;
	double lastProb = 0;
	for (int i = 0;i < 100;i++) {
	    if (prob < (lastProb + (Math.pow(0.9, i) * 0.1))) {
		ticks *= ((2 * i) + 3);
		break;
	    }
	    lastProb += (Math.pow(0.9, i) * 0.1);
	}
	System.out.println("ticks = " + ticks);
	return ticks;
    }
    
    /**
     * Entry point for the test.
     */
    public static void start(int processes) {
	Debug.println('+', "Entering StallingsTest");
	new StallingsTest(processes);
    }

}
